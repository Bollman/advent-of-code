#include <algorithm>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include "constsEnumsStructs.h"
#include "printer.h"

using namespace std;

int getHandType(const vector<int>& cards)
{
	int max = 0;
	map<int, int> maximums = {
		{c2,0},
		{c3,0},
		{c4,0},
		{c5,0},
		{c6,0},
		{c7,0},
		{c8,0},
		{c9,0},
		{cT,0},
		{cJ,0},
		{cQ,0},
		{cK,0},
		{cA,0}
	};

	for (auto& card : cards)
	{
		if (card == CARD::cJoker)
		{
			continue;
		}

		maximums[card]++;

		if (maximums[card] > max)
		{
			max = maximums[card];
		}
	}

	if (max == 5)
	{
		return HANDTYPE::fiveOfKind;
	}

	if (max == 4)
	{
		return HANDTYPE::fourOfKind;
	}

	if (max == 3)
	{
		for (auto& maximum : maximums)
		{
			if (maximum.second == 2)
			{
				return HANDTYPE::fullHouse;
			}
		}

		return HANDTYPE::threeOfKind;
	}

	if (max == 2)
	{
		int numberOfPairs = 0;

		for (auto& maximum : maximums)
		{
			if (maximum.second == 2)
			{
				numberOfPairs++;
			}
		}

		if (numberOfPairs == 2)
		{
			return HANDTYPE::twoPair;
		}
		else
		{
			return HANDTYPE::onePair;
		}
	}

	return HANDTYPE::highCard;
}

hand createHand(string line)
{
	hand hand;

	for (int i = 0; i < 5; i++)  // 5 cards in one hand
	{
		if (line[i] >= ascii0 && line[i] <= ascii9)
		{
			hand.cards.push_back(line[i] - ascii0);
		}
		else
		{
			switch (line[i])
			{
			case 'T':
				hand.cards.push_back(CARD::cT);
				break;

			case 'J':
				hand.cards.push_back(CARD::cJ);
				break;
			
			case 'Q':
				hand.cards.push_back(CARD::cQ);
				break;

			case 'K':
				hand.cards.push_back(CARD::cK);
				break;

			case 'A':
				hand.cards.push_back(CARD::cA);
				break;
			default:
				break;
			}
		}
	}

	hand.bid = stoi(line.substr(6, line.size())); // 6 is size of string "AAAAA "
	hand.handType = getHandType(hand.cards);

	return hand;
}

void jacksToJokers(vector<hand>& hands)
{
	for (auto& hand : hands)
	{
		for (auto& card : hand.cards)
		{
			if (card == CARD::cJ)
			{
				card = CARD::cJoker;
			}
		}
	}
}

void updateHandsTypesWithJokers(vector<hand>& hands)
{
	for (auto& hand : hands)
	{
		int numberOfJokers = 0;

		for (auto& card : hand.cards)
		{
			if (card == CARD::cJoker)
			{
				numberOfJokers++;
			}
		}
		
		hand.handType = getHandType(hand.cards);

		switch (numberOfJokers)
		{
		case 0:			
			break;

		case 1:
			if (hand.handType == HANDTYPE::highCard)
			{
				hand.handType = HANDTYPE::onePair;
			}
			else if (hand.handType == HANDTYPE::onePair)
			{
				hand.handType = HANDTYPE::threeOfKind;
			}
			else if (hand.handType == HANDTYPE::twoPair)
			{
				hand.handType = HANDTYPE::fullHouse;
			}
			else if (hand.handType == HANDTYPE::threeOfKind || hand.handType == HANDTYPE::fullHouse)
			{
				hand.handType = HANDTYPE::fourOfKind;
			}
			else
			{
				hand.handType = HANDTYPE::fiveOfKind;
			}
			break;

		case 2:
			if (hand.handType == HANDTYPE::highCard)
			{
				hand.handType = HANDTYPE::threeOfKind;
			}
			else if (hand.handType == HANDTYPE::onePair || hand.handType == HANDTYPE::twoPair)
			{
				hand.handType = HANDTYPE::fourOfKind;
			}
			else
			{
				hand.handType = HANDTYPE::fiveOfKind;
			}
			break;

		case 3:
			if (hand.handType == HANDTYPE::highCard)
			{
				hand.handType = HANDTYPE::fourOfKind;
			}
			else
			{
				hand.handType = HANDTYPE::fiveOfKind;
			}
			break;

		case 4:
			hand.handType = HANDTYPE::fiveOfKind;
			break;
		
		case 5:
			hand.handType = HANDTYPE::fiveOfKind;
			break;

		default:
			break;
		}
	}
}

int sumWinnings(const vector<hand>& hands)
{
	int sum = 0;

	for (int rank = 1; rank <= hands.size(); rank++)
	{
		sum += rank * hands[rank - 1].bid;
	}

	return sum;
}

void readFromIntput(const string inputPath, vector<hand>& hands)
{	
	ifstream inputFile;
	string line;

	inputFile.open(inputPath);

	if (inputFile.is_open())
	{
		while (getline(inputFile, line))
		{	
			hands.push_back(createHand(line));
		}
	}
	else
	{
		cout << "Failed to open the input file.\n";
	}

	inputFile.close();
}

int main()
{
	const string inputPath = "C:\\input.txt";
	vector<hand> hands;

	readFromIntput(inputPath, hands);
	sort(hands.begin(), hands.end());	
	cout << "          totalWinnings : " << sumWinnings(hands) << "\n";

	jacksToJokers(hands);
	updateHandsTypesWithJokers(hands);
	sort(hands.begin(), hands.end());
	cout << "totalWinningsWithJokers : " << sumWinnings(hands) << "\n";

	return 0;
}
