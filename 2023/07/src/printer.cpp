#include "printer.h"

void printHand(const hand hand)
{
	std::cout << "cards:\n";

	for (auto& card : hand.cards)
	{
		std::cout << "  " << card << "\n";
	}	

	switch (hand.handType)
	{
	case HANDTYPE::highCard:
		std::cout << "  High card\n";
		break;

	case HANDTYPE::onePair:
		std::cout << "  One pair\n";
		break;

	case HANDTYPE::twoPair:
		std::cout << "  Two pair\n";
		break;

	case HANDTYPE::threeOfKind:
		std::cout << "  Three of a kind\n";
		break;

	case HANDTYPE::fullHouse:
		std::cout << "  Full house\n";
		break;

	case HANDTYPE::fourOfKind:
		std::cout << "  Four of a kind\n";
		break;

	case HANDTYPE::fiveOfKind:
		std::cout << "  Five of a kind\n";
		break;

	default:
		break;
	}

	std::cout << "  bid: " << hand.bid << "\n\n";
}

void printHandsSimple(const std::vector<hand>& hands)
{
	for (auto& hand : hands)
	{
		for (auto& c : hand.cards)
		{
			std::cout << c << " ";
		}

		std::cout << "type: " << hand.handType << "\n";
	}

	std::cout << "\n";
}
