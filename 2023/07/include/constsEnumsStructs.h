#pragma once
#include <vector>

const int ascii0 = 48;
const int ascii9 = 57;

enum CARD
{
	cJoker = 1,
	c2,
	c3,
	c4,
	c5,
	c6,
	c7,
	c8,
	c9,
	cT,
	cJ,
	cQ,
	cK,
	cA
};

enum HANDTYPE
{
	highCard = 0,
	onePair,
	twoPair,
	threeOfKind,
	fullHouse,
	fourOfKind,
	fiveOfKind
};

struct hand
{
	std::vector<int> cards;
	int bid;
	int handType;

	bool operator < (const hand& h2) const
	{
		if (handType < h2.handType)
		{
			return true;
		}

		if (handType > h2.handType)
		{
			return false;
		}
		
		for (int i = 0; i < cards.size(); i++)
		{
			if (cards[i] < h2.cards[i])
			{
				return true;
			}

			if (cards[i] > h2.cards[i])
			{
				return false;
			}
		}
		
		return false;
	}
};
