#pragma once
#include <iostream>
#include <vector>
#include "constsEnumsStructs.h"

void printHand(const hand hand);

void printHandsSimple(const std::vector<hand>& hands);
