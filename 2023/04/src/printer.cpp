#include "printer.h"

void printCard(const card card)
{
	std::cout << "  winningNumbers : ";

	for (auto& number : card.winningNumbers)
	{
		std::cout << number << " ";
	}

	std::cout << "\n   havingNumbers : ";

	for (auto& number : card.havingNumbers)
	{
		std::cout << number << " ";
	}

	std::cout << "\n";
}

void printCards(const std::vector<card> cards)
{
	for (auto& card : cards)
	{
		printCard(card);
	}
}
