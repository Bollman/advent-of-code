#include <fstream>
#include <iostream>
#include <numeric>
#include <sstream>
#include <string>
#include "constsEnumsStructs.h"
#include "printer.h"

using namespace std;

int getCardmatches(const card card)
{
	int numberOfMatches = 0;

	for (auto& winningNumber : card.winningNumbers)
	{
		for (auto& havingNumber : card.havingNumbers)
		{
			if (winningNumber == havingNumber)
			{
				numberOfMatches++;
			}
		}
	}

	return numberOfMatches;
}

int getPointsFromCard(const card card)
{
	const int numberOfMatches = getCardmatches(card);

	if (numberOfMatches > 0)
	{
		return pow(2, numberOfMatches - 1);
	}

	return 0;
}

int sumPoints(const vector<card>& cards)
{
	int points = 0;

	for (auto& card : cards)
	{
		points += getPointsFromCard(card);
	}

	return points;
}

vector<int> splitToNumbers(const string input)
{
	vector<int> result;

	for (int i = 0; i < input.size(); i += 3)
	{
		int currNumber = 0;

		if (input[i] != ' ')
		{
			currNumber += 10 * (input[i] - asciiOffset);
		}

		currNumber += input[i + 1] - asciiOffset;
		result.push_back(currNumber);
	}

	return result;
}

int countAllCards(const vector<card>& cards)
{
	const int cardsSize = cards.size();
	vector<int> cardsStock(cardsSize, 1);

	for (int i = 0; i < cardsSize; i++)
	{
		const int currCardMatches = getCardmatches(cards[i]);

		for (int j = 0; j < cardsStock[i]; j++)
		{
			for (int k = i + 1; k <= i + currCardMatches; k++)
			{
				cardsStock[k]++;
			}
		}
	}	

	return accumulate(cardsStock.begin(), cardsStock.end(), 0);
}

void readFromIntput(const string inputPath, vector<card>& cards)
{	
	ifstream inputFile;
	string line;

	inputFile.open(inputPath);

	if (inputFile.is_open())
	{
		while (getline(inputFile, line))
		{
			card card;

			line = line.substr(line.find(": ", 0) + 2, line.size()); // 2 is length of string ": "
			string firstNumbers = line.substr(0, line.find(" | ", 0));
			string secondNumbers = line.substr(line.find(" | ", 0) + 3, line.size()); // 3 is length of string " | "	

			card.winningNumbers = splitToNumbers(firstNumbers);
			card.havingNumbers = splitToNumbers(secondNumbers);
			cards.push_back(card);
		}
	}
	else
	{
		cout << "Failed to open the input file.\n";
	}

	inputFile.close();
}

int main()
{
	const string inputPath = "C:\\input.txt";
	vector<card> cards;

	readFromIntput(inputPath, cards);

	cout << " sumPoints : " << sumPoints(cards) << "\n";
	cout << "totalCards : " << countAllCards(cards) << "\n";

	return 0;
}
