#pragma once
#include <vector>

enum INS
{
	LEFT,
	RIGHT
};

struct node
{
	std::string name;
	std::string left;
	std::string right;
};

struct map
{
	std::vector<int> instructions;
	std::vector<node> nodes;
};
