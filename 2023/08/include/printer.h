#pragma once
#include <iostream>
#include <vector>
#include "constsEnumsStructs.h"

void printInstructions(std::vector<int> instructions);

void printNode(const node node);

void printNodes(const std::vector<node>& nodes);
