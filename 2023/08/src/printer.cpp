#include "printer.h"

void printInstructions(std::vector<int> instructions)
{
	for (auto& ins : instructions)
	{
		if (ins == INS::LEFT)
		{
			std::cout << "L";
		}
		else if (ins == INS::RIGHT)
		{
			std::cout << "R";
		}
	}

	std::cout << "\n";
}

void printNode(const node node)
{
	std::cout << node.name << " = (" << node.left << ", " << node.right << ")\n";
}

void printNodes(const std::vector<node>& nodes)
{
	for (auto& node : nodes)
	{
		printNode(node);
	}

	std::cout << "\n";
}
