#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include "constsEnumsStructs.h"
#include "printer.h"

using namespace std;

int findNodeByName(const vector<node>& nodes, const string name)
{
	for (int i = 0; i < nodes.size(); i++)
	{
		if (name == nodes[i].name)
		{
			return i;
		}
	}

	cout << "Node not found\n";
	return -1;
}

int countStepsFromOneNode(const map& map, node& currNode, const bool ghostMode)
{
	int steps = 0;
	int insIndex = 0;

	while (
		(!ghostMode && currNode.name != "ZZZ") ||
		(ghostMode && currNode.name[2] != 'Z')
		)
	{
		if (map.instructions[insIndex] == INS::LEFT)
		{
			currNode = map.nodes[findNodeByName(map.nodes, currNode.left)];
		}
		else if (map.instructions[insIndex] == INS::RIGHT)
		{
			currNode = map.nodes[findNodeByName(map.nodes, currNode.right)];
		}

		steps++;

		if (insIndex < map.instructions.size() - 1)
		{
			insIndex++;
		}
		else
		{
			insIndex = 0;
		}
	}

	return steps;
}

int64_t countLCM(const int64_t numA, const int64_t numB)
{
	int64_t greater = max(numA, numB);
	int64_t smaller = min(numA, numB);

	for (int64_t i = greater; ; i += greater)
	{
		if (i % smaller == 0)
		{
			return i;
		}
	}
}

int64_t countStepsToReachFinish(const map& map, const bool ghostMode)
{
	if (!ghostMode)
	{
		node currNode = map.nodes[findNodeByName(map.nodes, "AAA")];
		return countStepsFromOneNode(map, currNode, ghostMode);
	}
	else
	{
		vector<int> stepsCounts;

		for (auto currNode : map.nodes)
		{
			if (currNode.name[2] == 'A')
			{				
				stepsCounts.push_back(countStepsFromOneNode(map, currNode, ghostMode));
			}
		}	

		int64_t result = stepsCounts[0];

		for (int i = 1; i < stepsCounts.size(); i++)
		{
			result = countLCM(result, stepsCounts[i]);
		}

		return result;
	}
}

void readFromIntput(const string inputPath, map& map)
{	
	ifstream inputFile;
	string line;

	inputFile.open(inputPath);

	if (inputFile.is_open())
	{
		getline(inputFile, line);
		for (auto& c : line)
		{
			if (c == 'L')
			{
				map.instructions.push_back(INS::LEFT);
			}
			
			if (c == 'R')
			{
				map.instructions.push_back(INS::RIGHT);
			}
		}

		getline(inputFile, line); // skip expected empty line

		while (getline(inputFile, line))
		{
			node node;
			node.name = line.substr(0, 3);
			node.left = line.substr(7, 3);
			node.right = line.substr(12, 3);
			map.nodes.push_back(node);
		}
	}
	else
	{
		cout << "Failed to open the input file.\n";
	}

	inputFile.close();
}

int main()
{
	const string inputPath = "C:\\input.txt";
	map map;

	readFromIntput(inputPath, map);
	cout << "stepsForHuman : " << countStepsToReachFinish(map, false) << "\n";
	cout << "stepsForGhost : " << countStepsToReachFinish(map, true) << "\n";

	return 0;
}
