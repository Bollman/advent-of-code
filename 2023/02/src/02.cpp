#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include "constsEnumsStructs.h"
#include "printer.h"

using namespace std;

vector<string> split(string & str, const string token) {
	vector<string>result;
	while (str.size()) {
		int index = str.find(token);
		if (index != string::npos) {
			result.push_back(str.substr(0, index));
			str = str.substr(index + token.size());
			if (str.size() == 0)result.push_back(str);
		}
		else {
			result.push_back(str);
			str = "";
		}
	}
	return result;
}

bool isGamePossible(const game game, const int redMax, const int greenMax, const int blueMax)
{
	for (auto& gameSet : game.gameSets)
	{
		if (gameSet.red > redMax)
		{
			return false;
		}

		if (gameSet.green > greenMax)
		{
			return false;
		}

		if (gameSet.blue > blueMax)
		{
			return false;
		}
	}

	return true;
}

int sumPossibleGames(const vector<game>& games, const int redMax, const int greenMax, const int blueMax)
{
	int sum = 0;

	for (auto& game : games)
	{
		if (isGamePossible(game, redMax, greenMax, blueMax))
		{
			sum += game.gameNumber;
		}
	}

	return sum;
}

int getGamePower(const game game)
{
	int redMax = 1;
	int greenMax = 1;
	int blueMax = 1;

	for (auto& gameSet : game.gameSets)
	{
		if (gameSet.red > redMax)
		{
			redMax = gameSet.red;
		}

		if (gameSet.green > greenMax)
		{
			greenMax = gameSet.green;
		}

		if (gameSet.blue > blueMax)
		{
			blueMax = gameSet.blue;
		}
	}

	return redMax * greenMax * blueMax;
}

int sumPowers(const vector<game>& games)
{
	int sum = 0;

	for (auto& game : games)
	{
		sum += getGamePower(game);
	}

	return sum;
}

void readFromIntput(const string inputPath, vector<game>& games)
{	
	const int gameStringLength = 5; // length of string "Game "

	ifstream inputFile;
	string line;
	string gameNumber;

	inputFile.open(inputPath);

	if (inputFile.is_open())
	{
		while (getline(inputFile, line))
		{
			// get game number
			game game;			
			gameNumber = line.substr(gameStringLength, line.size() - gameStringLength); // cut "Game " in the beginning
			gameNumber = gameNumber.substr(0, gameNumber.find(':', 0)); // extract number
			game.gameNumber = stoi(gameNumber);

			// split line to game sets
			string gameSetsString = line.substr(line.find(": ")+2, line.size() - line.find(": ") - 1);
			vector<string> gameSetsStrings = split(gameSetsString, "; ");			
		
			//fill game sets to structures
			for (auto& gameSetString : gameSetsStrings)
			{
				gameSet currGameSet;
				vector<string> cubesTakenSet = split(gameSetString, ", ");

				for (auto& cubesTaken : cubesTakenSet)
				{
					const int redStringLength = 4;   // length of string " red"
					const int greenStringLength = 6; // length of string " green"
					const int blueStringLength = 5;  // length of string " blue"

					if (cubesTaken.find("red") != string::npos)
					{
						currGameSet.red = stoi(cubesTaken.substr(0, cubesTaken.size() - redStringLength));
					}
					else if (cubesTaken.find("green") != string::npos)
					{
						currGameSet.green = stoi(cubesTaken.substr(0, cubesTaken.size() - greenStringLength));
					}
					else if (cubesTaken.find("blue") != string::npos)
					{
						currGameSet.blue = stoi(cubesTaken.substr(0, cubesTaken.size() - blueStringLength));
					}

				}

				game.gameSets.push_back(currGameSet);
			}

			games.push_back(game);
		}
	}
	else
	{
		cout << "Failed to open the input file.\n";
	}

	inputFile.close();
}

int main()
{
	const int REDLIMIT = 12;
	const int GREENLIMIT = 13;
	const int BLUELIMIT = 14;
	const string inputFilePath = "C:\\input.txt";

	vector<game> games;
	readFromIntput(inputFilePath, games);

	cout << "sumPossibleGames : " << sumPossibleGames(games, REDLIMIT, GREENLIMIT, BLUELIMIT) << "\n";
	cout << "       sumPowers : " << sumPowers(games) << "\n";

	return 0;
}
