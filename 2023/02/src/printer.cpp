#include "printer.h"

void printGameSet(const gameSet gameSet)
{
	if (gameSet.red > 0)
	{
		std::cout << "  " << gameSet.red << " red\n";
	}

	if (gameSet.green > 0)
	{
		std::cout << "  " << gameSet.green << " green\n";
	}

	if (gameSet.blue > 0)
	{
		std::cout << "  " << gameSet.blue << " blue\n";
	}

	std::cout << "\n";
}

void printGame(const game game)
{
	std::cout << "Game " << game.gameNumber << "\n";

	for (auto& set : game.gameSets)
	{
		printGameSet(set);
	}

	std::cout << "\n";
}

void printGames(const std::vector<game>& games)
{
	for (auto& game : games)
	{
		printGame(game);
	}
}
