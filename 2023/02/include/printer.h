#pragma once
#include <iostream>
#include <vector>
#include "constsEnumsStructs.h"

void printGameSet(const gameSet gameSet);

void printGame(const game game);

void printGames(const std::vector<game>& games);
