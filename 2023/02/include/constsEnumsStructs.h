#pragma once
#include <vector>

struct gameSet {
	
	int red = 0;
	int green = 0;
	int blue = 0;
};

struct game {

	int gameNumber;
	std::vector<gameSet> gameSets;
};