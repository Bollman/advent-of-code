#pragma once
#include <vector>

const int ascii0 = 48;
const int ascii9 = 57;

struct race
{
	int64_t time;
	int64_t distance;
};
