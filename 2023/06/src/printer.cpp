#include "printer.h"

void printRaces(const std::vector<race> races)
{
	std::cout << "Races:\n";

	for (auto& race : races)
	{
		std::cout << "  time: " << race.time << "   distance: " << race.distance << "\n";
	}
}
