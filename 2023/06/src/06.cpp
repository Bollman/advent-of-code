#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include "constsEnumsStructs.h"
#include "printer.h"

using namespace std;

vector<int64_t> spaceSplit(const string& str)
{
	int64_t currNumber = 0;
	vector<int64_t> result;

	for (auto& c : str)
	{
		if (c >= ascii0 && c <= ascii9)
		{
			currNumber = 10 * currNumber + (c - ascii0);			
		}
		else
		{	
			if (currNumber == 0)
			{
				continue;
			}

			result.push_back(currNumber);
			currNumber = 0;			
		}		
	}

	result.push_back(currNumber);

	return result;
}

int64_t stringToInt64(const string str)
{
	int64_t result = 0;

	for (int i = 0; i < str.size(); i++)
	{
		result = 10 * result + (str[i] - ascii0);
	}

	return result;
}

int64_t getWaysOfRecord(race race)
{
	int64_t waysOfRecord = 0;

	for (int64_t holdTime = 0; holdTime < race.time; holdTime++)
	{	
		int64_t initSpeed = holdTime;
		int64_t driveTime = race.time - holdTime;
		
		if (driveTime * initSpeed > race.distance)
		{
			waysOfRecord++;
		}		 
	}

	return waysOfRecord;
}

int64_t multiplyWaysOfRecord(const vector<race>& races)
{
	int64_t result = 1;

	for (auto& race : races)
	{
		result *= getWaysOfRecord(race);
	}

	return result;
}

race transformRacesToOneRace(const vector<race>& races)
{
	string raceTime = "";
	string raceDistance = "";

	for (auto& race : races)
	{
		raceTime += to_string(race.time);
		raceDistance += to_string(race.distance);
	}

	return { stringToInt64(raceTime),stringToInt64(raceDistance)};
}

void readFromIntput(const string inputPath, vector<race>& races)
{	
	ifstream inputFile;
	string line;

	inputFile.open(inputPath);

	if (inputFile.is_open())
	{
		vector<int64_t> times;
		vector<int64_t> distances;
		
		getline(inputFile, line);
		line = line.substr(11, line.size()); // 11 is legth of string "Time:      "
		times = spaceSplit(line);	

		getline(inputFile, line);
		line = line.substr(11, line.size()); // 11 is legth of string "Distance:  "
		distances = spaceSplit(line);

		for (int i = 0; i < times.size(); i++)
		{
			races.push_back({times[i], distances[i]});
		}
	}
	else
	{
		cout << "Failed to open the input file.\n";
	}

	inputFile.close();
}

int main()
{
	const string inputPath = "C:\\input.txt";
	vector<race> races;
	race theOneRace;

	readFromIntput(inputPath, races);
	theOneRace = transformRacesToOneRace(races);

	cout << "multipliedWaysOfRecord : " << multiplyWaysOfRecord(races) << "\n";
	cout << "   oneRaceWaysOfRecord : " << getWaysOfRecord(theOneRace) << "\n";

	return 0;
}
