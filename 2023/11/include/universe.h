#pragma once
#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "constsEnumsStructs.h"

class Universe
{
public:
	Universe(const std::string inputPath);
	~Universe();

	void print();
	void printWithGalaxiesNumbers();
	void expand();
	void findGalaxies();
	int  sumPathsBetweenGalaxies();

private:
	std::vector<std::vector<int>> universeMap;
	std::vector<galaxy> galaxies;

	int galaxyIsThere(const int line, const int column);
	int countPathBetweenGalaxies(galaxy galaxyOne, galaxy galaxyTwo);
};
