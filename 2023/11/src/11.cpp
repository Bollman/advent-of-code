#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include "constsEnumsStructs.h"
#include "universe.h"

using namespace std;

int main()
{
	const string inputFilePath = "C:\\input.txt";
	Universe universe = Universe(inputFilePath);

	universe.expand();
	universe.findGalaxies();
	cout << "sumPathsBetweenGalaxies : " << universe.sumPathsBetweenGalaxies() << "\n";

	return 0;
}
