#include "universe.h"

Universe::Universe(const std::string inputPath)
{
	std::cout << "Universe created\n";

	std::ifstream inputFile;
	std::string line;

	inputFile.open(inputPath);

	if (inputFile.is_open())
	{
		while (getline(inputFile, line))
		{
			std::vector<int> newLine;

			for (auto& c : line)
			{
				if (c == '.')
				{
					newLine.push_back(MapMark::FREESPACE);
				}
				if (c == '#')
				{
					newLine.push_back(MapMark::GALAXY);
				}
			}

			universeMap.push_back(newLine);
		}
	}
	else
	{
		std::cout << "Failed to open the input file.\n";
	}

	inputFile.close();
}

Universe::~Universe()
{
	std::cout << "Universe destroyed\n";
}

void Universe::print()
{
	for (auto& line : universeMap)
	{
		for (auto& c : line)
		{
			if (c == MapMark::FREESPACE)
			{
				std::cout << ".";
			}
			if (c == MapMark::GALAXY)
			{
				std::cout << "#";
			}
		}

		std::cout << "\n";
	}
}

void Universe::printWithGalaxiesNumbers()
{
	for (int i = 0; i < universeMap.size(); ++i)
	{
		for (int j = 0; j < universeMap[i].size(); ++j)
		{
			int galaxyNumber = galaxyIsThere(i, j);

			if (galaxyNumber)
			{
				std::cout << galaxyNumber;
			}
			else
			{
				std::cout << ".";
			}
		}

		std::cout << "\n";
	}
}

void Universe::expand()
{
	// expand lines
	auto isLineEmpty = [&](int lineNumber)
		{
			for (auto& c : universeMap[lineNumber])
			{
				if (c == MapMark::GALAXY)
				{
					return false;
				}
			}

			return true;
		};

	for (int i = 0; i < universeMap.size(); ++i)
	{
		if (isLineEmpty(i))
		{
			std::vector<int> emptyLine(universeMap[i].size(), MapMark::FREESPACE);
			universeMap.insert(universeMap.begin() + i + 1, emptyLine);
			i++;
		}
	}
	
	// expand columns
	auto isColumnEmpty = [&](int columnNumber)
		{
			for (auto& line : universeMap)
			{
				if (line[columnNumber] == MapMark::GALAXY)
				{
					return false;
				}
			}

			return true;
		};

	for (int j = 0; j < universeMap[0].size(); ++j)
	{
		if (isColumnEmpty(j))
		{
			for (auto& line : universeMap)
			{
				line.insert(line.begin() + j + 1, MapMark::FREESPACE);
			}
			j++;
		}
	}
}

void Universe::findGalaxies()
{
	int galaxyCount = 1;

	for (int i = 0; i < universeMap.size(); ++i)
	{
		for (int j = 0; j < universeMap[i].size(); ++j)
		{
			if (universeMap[i][j] == MapMark::GALAXY)
			{
				galaxies.push_back({ i, j, galaxyCount });
				galaxyCount++;
			}
		}
	}
}

int Universe::sumPathsBetweenGalaxies()
{
	int pathsSum = 0;

	for (int i = 0; i < galaxies.size(); ++i)
	{
		for (int j = i + 1; j < galaxies.size(); ++j)
		{
			pathsSum += countPathBetweenGalaxies( galaxies[i], galaxies[j]);
		}
	}

	return pathsSum;
}

int Universe::galaxyIsThere(const int line, const int column)
{
	for (auto& galaxy : galaxies)
	{
		if (galaxy.line == line && galaxy.column == column)
		{
			return galaxy.number;
		}
	}	

	return 0;
}

int Universe::countPathBetweenGalaxies(galaxy galaxyOne, galaxy galaxyTwo)
{
	return std::abs(galaxyTwo.line - galaxyOne.line) + std::abs(galaxyTwo.column - galaxyOne.column);
}
