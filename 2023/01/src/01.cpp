#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include "constsEnumsStructs.h"
#include "printer.h"

using namespace std;

digit getDigitFromLine(const string line, const ORIENTATION orientation, const DIGITTYPE digitType)
{
	digit digit;

	if (digitType == DIGITTYPE::NUMERIC)
	{
		if (orientation == ORIENTATION::FROMBEGGINIG)
		{
			for (int i = 0; i < line.size(); i++)
			{
				if (line[i] >= ascii0 && line[i] <= ascii9)
				{
					return { line[i] - ascii0, orientation, i };
				}
			}
		}
		else if (orientation == ORIENTATION::FROMBACK)
		{
			for (int i = line.size() - 1; i >= 0; i--)
			{
				if (line[i] >= ascii0 && line[i] <= ascii9)
				{
					return { line[i] - ascii0, orientation, i };
				}
			}
		}
	}
	else if (digitType == DIGITTYPE::WORD)
	{
		if (orientation == ORIENTATION::FROMBEGGINIG)
		{
			for (int i = 0; i < line.size(); i++)
			{
				if (line.substr(i, 3) == "one")
					return { 1, orientation, i };
				if (line.substr(i, 3) == "two")
					return { 2, orientation, i };
				if (line.substr(i, 5) == "three")
					return { 3, orientation, i };
				if (line.substr(i, 4) == "four")
					return { 4, orientation, i };
				if (line.substr(i, 4) == "five")
					return { 5, orientation, i };
				if (line.substr(i, 3) == "six")
					return { 6, orientation, i };
				if (line.substr(i, 5) == "seven")
					return { 7, orientation, i };
				if (line.substr(i, 5) == "eight")
					return { 8, orientation, i };
				if (line.substr(i, 4) == "nine")
					return { 9, orientation, i };
			}
		}
		else if (orientation == ORIENTATION::FROMBACK)
		{
			for (int i = line.size() - 1; i >= 0; i--)
			{
				if (line.substr(i, 3) == "one")
					return { 1, orientation, i };
				if (line.substr(i, 3) == "two")
					return { 2, orientation, i };
				if (line.substr(i, 5) == "three")
					return { 3, orientation, i };
				if (line.substr(i, 4) == "four")
					return { 4, orientation, i };
				if (line.substr(i, 4) == "five")
					return { 5, orientation, i };
				if (line.substr(i, 3) == "six")
					return { 6, orientation, i };
				if (line.substr(i, 5) == "seven")
					return { 7, orientation, i };
				if (line.substr(i, 5) == "eight")
					return { 8, orientation, i };
				if (line.substr(i, 4) == "nine")
					return { 9, orientation, i };
			}
		}
	}

	return { -1, -1 ,-1 };
}

int getNumericValueFromLine(const string line)
{
	digit firstDigit = getDigitFromLine(line, ORIENTATION::FROMBEGGINIG, DIGITTYPE::NUMERIC);
	digit lastDigit = getDigitFromLine(line, ORIENTATION::FROMBACK, DIGITTYPE::NUMERIC);

	return 10 * firstDigit.value + lastDigit.value;
}

int getNumericAndWordValueFromLine(const string line)
{
	digit firstDigitNumeric = getDigitFromLine(line, ORIENTATION::FROMBEGGINIG, DIGITTYPE::NUMERIC);
	digit lastDigitNumeric = getDigitFromLine(line, ORIENTATION::FROMBACK, DIGITTYPE::NUMERIC);

	digit firstDigitWord = getDigitFromLine(line, ORIENTATION::FROMBEGGINIG, DIGITTYPE::WORD);
	digit lastDigitWord = getDigitFromLine(line, ORIENTATION::FROMBACK, DIGITTYPE::WORD);

	int chosenFirst;
	int chosenLast;

	if (firstDigitWord.value != -1 && firstDigitWord.position < firstDigitNumeric.position)
	{
		chosenFirst = firstDigitWord.value;
	}
	else
	{
		chosenFirst = firstDigitNumeric.value;
	}

	if (lastDigitWord.value != -1 && lastDigitWord.position > lastDigitNumeric.position)
	{
		chosenLast = lastDigitWord.value;
	}
	else
	{
		chosenLast = lastDigitNumeric.value;
	}

	return 10 * chosenFirst + chosenLast;
}

int getValueFromLine(const string line, const DIGITTYPE digitType)
{
	if (digitType == DIGITTYPE::NUMERIC)
	{
		return(getNumericValueFromLine(line));
	}
	else if (digitType == DIGITTYPE::WORD)
	{
		return(getNumericAndWordValueFromLine(line));
	}
}

int sumCalibrationValues(const vector<string>& calibratinDocument, const DIGITTYPE digitType)
{
	int calibrationValuesSum = 0;

	for (auto& calibrationLine : calibratinDocument)
	{
		calibrationValuesSum += getValueFromLine(calibrationLine, digitType);
	}	

	return calibrationValuesSum;
}

void readFromIntput(const string inputPath, vector<string>& calibratinDocument)
{
	ifstream inputFile;
	string line;

	inputFile.open(inputPath);

	if (inputFile.is_open())
	{
		while (getline(inputFile, line))
		{
			calibratinDocument.push_back(line);
		}
	}
	else
	{
		cout << "Failed to open the input file.\n";
	}

	inputFile.close();
}

int main()
{
	const string inputFilePath = "C:\\input.txt";
	vector<string> calibratinDocument;

	readFromIntput(inputFilePath, calibratinDocument);
	cout << "numeric sumCalibrationValues = " << sumCalibrationValues(calibratinDocument, DIGITTYPE::NUMERIC) << "\n";
	cout << "   word sumCalibrationValues = " << sumCalibrationValues(calibratinDocument, DIGITTYPE::WORD) << "\n";

	return 0;
}
