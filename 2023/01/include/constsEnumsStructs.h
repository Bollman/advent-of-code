#pragma once

const int ascii0 = 48;
const int ascii9 = 57;

enum DIGITTYPE
{
	NUMERIC,
	WORD
};

enum ORIENTATION
{
	FROMBEGGINIG,
	FROMBACK
};

struct digit
{
	int value;
	int orientation;
	int position;
};
