#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include "constsEnumsStructs.h"
#include "printer.h"

using namespace std;

bool isSequenceZerosOnly(const vector<int>& sequence)
{
	for (auto& number : sequence)
	{
		if (number != 0)
		{
			return false;
		}
	}

	return true;
}

void addDifferencesToHistory(history& history)
{
	int lastSequenceIndex = 0;

	while (!isSequenceZerosOnly(history.sequences[lastSequenceIndex]))
	{
		vector<int> newSequence;

		for (int i = 0; i < history.sequences[lastSequenceIndex].size() - 1; i++)
		{
			newSequence.push_back(history.sequences[lastSequenceIndex][i + 1] - history.sequences[lastSequenceIndex][i]);
		}

		history.sequences.push_back(newSequence);
		lastSequenceIndex++;
	}
}

void addDifferencesToHistories(vector<history>& histories)
{
	for (auto& history : histories)
	{
		addDifferencesToHistory(history);
	}
}

void extrapolateHistory(history& history, const DIRECTION direction)
{
	// add one Zero to the last sequence
	history.sequences[history.sequences.size() - 1].push_back(0);

	for (int i = history.sequences.size() - 2; i >= 0; i--)
	{
		if (direction == DIRECTION::FORWARD)
		{
			history.sequences[i].push_back(history.sequences[i].back() + history.sequences[i + 1].back());
		}
		else if (direction == DIRECTION::BACKWARD)
		{
			history.sequences[i].insert(history.sequences[i].begin(), history.sequences[i].front() - history.sequences[i + 1].front());
		}
	}
}

void extrapolateHistories(vector<history>& histories, const DIRECTION direction)
{
	for (auto& history : histories)
	{
		extrapolateHistory(history, direction);
	}
}

int sumExtrapolatedValues(const vector<history>& histories, const DIRECTION direction)
{
	int sum = 0;

	for (auto& history : histories)
	{
		if (direction == DIRECTION::FORWARD)
		{
			sum += history.sequences[0].back();
		}
		else if (direction == DIRECTION::BACKWARD)
		{
			sum += history.sequences[0].front();
		}
	}

	return sum;
}

void readFromIntput(const string inputPath, vector<history>& histories)
{
	ifstream inputFile;
	string line;

	inputFile.open(inputPath);

	if (inputFile.is_open())
	{
		while (getline(inputFile, line))
		{
			string s;
			stringstream ss(line);
			vector <string> numbersString;
			vector <int> numbers;
			history newHistory;

			while (getline(ss, s, ' ')) {
				numbersString.push_back(s);
			}

			for (auto& number : numbersString)
			{
				numbers.push_back(stoi(number));
			}

			newHistory.sequences.push_back(numbers);
			histories.push_back(newHistory);
		}
	}
	else
	{
		cout << "Failed to open the input file.\n";
	}

	inputFile.close();
}

int main()
{
	const string inputPath = "C:\\input.txt";
	vector<history> histories;

	readFromIntput(inputPath, histories);
	addDifferencesToHistories(histories);
	extrapolateHistories(histories, DIRECTION::FORWARD);
	extrapolateHistories(histories, DIRECTION::BACKWARD);

	cout << "sumExtrapolatedValues : " << sumExtrapolatedValues(histories, DIRECTION::FORWARD) << "\n";
	cout << "sumExtrapolatedValues : " << sumExtrapolatedValues(histories, DIRECTION::BACKWARD) << "\n";

	return 0;
}
