#include "printer.h"

void printHistory(const history history)
{
	for (auto& sequence : history.sequences)
	{
		for (auto& number : sequence)
		{
			std::cout << number << " ";
		}

		std::cout << "\n";
	}
}

void printHistories(const std::vector<history> histories)
{
	for (auto& history : histories)
	{
		printHistory(history);
	}
}
