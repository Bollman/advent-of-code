#pragma once
#include <vector>

const int ascii0 = 48;

enum DIRECTION
{
	FORWARD,
	BACKWARD
};

struct history
{
	std::vector<std::vector<int>> sequences;
};
