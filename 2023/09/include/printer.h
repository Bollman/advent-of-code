#pragma once
#include <iostream>
#include <vector>
#include "constsEnumsStructs.h"

void printHistory(const history history);

void printHistories(const std::vector<history> histories);
