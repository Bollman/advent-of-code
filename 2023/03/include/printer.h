#pragma once
#include <iostream>
#include <vector>
#include "constsEnumsStructs.h"

void printEngineSchematic(const std::vector<std::vector<char>>& engineSchematic);

void printSymbol(const symbolCoordinates& symbol);

void printSymbols(const std::vector<symbolCoordinates>& symbols);

void printNumber(const numberCoordinates& number);

void printNumbers(const std::vector<numberCoordinates>& numbers);
