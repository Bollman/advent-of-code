#pragma once

const int asciiNumbersOffset = 48;
const int amountOfNumbersAttachedToGear = 2;

struct symbolCoordinates
{
	int line;
	int column;
	char value;
};

struct numberCoordinates
{
	int line;
	int column;
	int length;
	int value;
};
