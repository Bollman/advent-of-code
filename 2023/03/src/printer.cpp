#include "printer.h"

void printEngineSchematic(const std::vector<std::vector<char>>& engineSchematic)
{
	for (auto& engineSchematicLine : engineSchematic)
	{
		for (auto& c : engineSchematicLine)
		{
			std::cout << c;
		}
		std::cout << "\n";
	}
}

void printSymbol(const symbolCoordinates& symbol)
{
	std::cout << "[" << symbol.line << ", " << symbol.column << "] val: " << symbol.value << "\n";
}

void printSymbols(const std::vector<symbolCoordinates>& symbols)
{
	for (auto& symbol : symbols)
	{
		printSymbol(symbol);
	}
}

void printNumber(const numberCoordinates& number)
{
	std::cout << "[" << number.line << ", " << number.column << "] len: " << number.length << " val: " << number.value << "\n";
}

void printNumbers(const std::vector<numberCoordinates>& numbers)
{
	for (auto& number : numbers)
	{
		printNumber(number);
	}
}
