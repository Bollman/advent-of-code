#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include "constsEnumsStructs.h"
#include "printer.h"

using namespace std;

bool isNumberAdjacentToSymbol(const numberCoordinates number, const symbolCoordinates symbol)
{
	//symbol is to the left
	if (number.line == symbol.line && number.column == (symbol.column + 1))
	{
		return true;
	}

	//symbol is to the right 
	if (number.line == symbol.line && number.column == (symbol.column - number.length))
	{
		return true;
	}

	//symbol is above
	if (number.line == (symbol.line + 1) &&
		number.column <= (symbol.column + 1) && (number.column + number.length) >= symbol.column)
	{
		return true;
	}

	//symbol is bellow
	if (number.line == (symbol.line - 1) &&
		number.column <= (symbol.column + 1) &&	(number.column + number.length) >= symbol.column)
	{
		return true;
	}

	return false;
}

int sumGearsNumbers(const vector<symbolCoordinates>& symbols, const vector<numberCoordinates>& numbers)
{
	int sum = 0;

	for (auto& symbol : symbols)
	{
		if (symbol.value == '*')
		{
			int adjacentCount = 0;
			int gearRatio = 1;

			for (auto& number : numbers)
			{
				if (isNumberAdjacentToSymbol(number, symbol))
				{
					adjacentCount++;
					gearRatio *= number.value;
				}			
			}

			if (adjacentCount == 2)
			{
				sum += gearRatio;
			}
		}
	}

	return sum;
}

int sumNumbersInEngineSchematic(const vector<symbolCoordinates>& symbols, const vector<numberCoordinates>& numbers)
{
	int engineSchematicSum = 0;

	for (auto& number : numbers)
	{
		for (auto& symbol : symbols)
		{
			if (isNumberAdjacentToSymbol(number, symbol))
			{
				engineSchematicSum += number.value;
			}
		}
	}

	return engineSchematicSum;
}

void findSymbolsAndNumbers(const vector<vector<char>>& engineSchematic, vector<symbolCoordinates>& symbols, vector<numberCoordinates>& numbers)
{
	for (int i = 0; i < engineSchematic.size(); ++i)
	{
		for (int j = 0; j < engineSchematic[i].size(); ++j)
		{
			char currentSchemaPart = engineSchematic[i][j];

			if (currentSchemaPart != '.'  && (currentSchemaPart < '0'  || currentSchemaPart > '9'))
			{				
				symbols.push_back({ i,j,currentSchemaPart });
				continue;
			}

			if (currentSchemaPart >= '0' && currentSchemaPart <= '9')
			{
				int currentNumber = currentSchemaPart - asciiNumbersOffset;
				int currentNumberLength = 1;				

				while (j + currentNumberLength < engineSchematic[i].size() &&
						(engineSchematic[i][j + currentNumberLength] >= '0' && engineSchematic[i][j + currentNumberLength] <= '9'))
				{				
					currentNumber *= 10;
					currentNumber += (engineSchematic[i][j + currentNumberLength] - asciiNumbersOffset);
					currentNumberLength++;
				}

				numbers.push_back({ i,j,currentNumberLength,currentNumber });
				j += (currentNumberLength - 1);
			}
		}
	}
}

void readFromIntput(const string inputPath, vector<vector<char>>& engineSchematic)
{
	ifstream inputFile;
	string line;

	inputFile.open(inputPath);

	if (inputFile.is_open())
	{
		while (getline(inputFile, line))
		{
			vector<char> engineSchematicLine(line.begin(), line.end());
			engineSchematic.push_back(engineSchematicLine);
		}
	}
	else
	{
		cout << "Failed to open the input file.\n";
	}

	inputFile.close();
}

int main()
{
	const string inputFilePath = "C:\\input.txt";
	vector<vector<char>> engineSchematic;
	vector<symbolCoordinates> symbols;
	vector<numberCoordinates> numbers;

	readFromIntput(inputFilePath, engineSchematic);
	findSymbolsAndNumbers(engineSchematic, symbols, numbers);

	cout << "engineSchematicsSum : " << sumNumbersInEngineSchematic(symbols, numbers) << "\n";
	cout << "     gearsNumberSum : " << sumGearsNumbers(symbols, numbers) << "\n";

	return 0;
}
