#pragma once
#include <iostream>
#include <vector>
#include "constsEnumsStructs.h"

void printSeeds(const std::vector<seed> seeds);

void printAlmanacMap(const std::vector<conversion> AlmanacMap);

void printAlmanacMaps(const almanacMaps almanacMaps);
