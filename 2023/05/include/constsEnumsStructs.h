#pragma once
#include <vector>

const int ascii0 = 48;
const int ascii9 = 57;

enum state
{
	SEEDS,
	SOIL,
	FERTILIZER,
	WATER,
	LIGHT,
	TEMPERATURE,
	HUMIDITY,
	LOCATION
};

struct seed
{
	int64_t number;
	int64_t destination;
};

struct conversion
{
	int64_t source;
	int64_t destination;
	int64_t range;
};

struct almanacMaps
{
	std::vector<conversion> seedToSoil;
	std::vector<conversion> soilToFertilizer;
	std::vector<conversion> fertilizerToWater;
	std::vector<conversion> waterToLight;
	std::vector<conversion> lightToTemperature;
	std::vector<conversion> temperatureToHumidity;
	std::vector<conversion> humidityToLocation;
};