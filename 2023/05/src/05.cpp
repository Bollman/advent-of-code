#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include "constsEnumsStructs.h"
#include "printer.h"

using namespace std;

vector<int64_t> spaceSplit(const string& str)
{
	int64_t currNumber = 0;
	vector<int64_t> result;

	for (auto& c : str)
	{
		if (c >= ascii0 && c <= ascii9)
		{
			currNumber = 10 * currNumber + (c - ascii0);
		}
		else
		{
			result.push_back(currNumber);
			currNumber = 0;
		}
	}

	result.push_back(currNumber);
	return result;
}

void processSeedsToVector(string inputSeeds, vector<seed>& outputSeeds)
{
	vector<int64_t> tmp = spaceSplit(inputSeeds);

	for (auto& number : tmp)
	{
		outputSeeds.push_back({number,number});
	}
}

conversion lineToConversion(string line)
{	
	vector<int64_t> tmp = spaceSplit(line);
	conversion conversion = { tmp[1], tmp[0], tmp[2] };
	return conversion;
}

void processSeedByConversion(seed& seed, const vector<conversion>& conversions)
{
	for (auto& conversion : conversions)
	{
		if (seed.destination >= conversion.source && seed.destination < conversion.source + conversion.range)
		{
			seed.destination = seed.destination + (conversion.destination - conversion.source);
			return;
		}
	}
}

void findLocationForSeed(seed& seed, const almanacMaps& almanacMaps)
{
	processSeedByConversion(seed, almanacMaps.seedToSoil);
	processSeedByConversion(seed, almanacMaps.soilToFertilizer);
	processSeedByConversion(seed, almanacMaps.fertilizerToWater);
	processSeedByConversion(seed, almanacMaps.waterToLight);
	processSeedByConversion(seed, almanacMaps.lightToTemperature);
	processSeedByConversion(seed, almanacMaps.temperatureToHumidity);
	processSeedByConversion(seed, almanacMaps.humidityToLocation);
}

void findLocationForSeeds(vector<seed>& seeds, const almanacMaps& almanacMaps)
{
	for (auto& seed : seeds)
	{
		findLocationForSeed(seed, almanacMaps);
	}
}

int64_t findClosestLocation(const vector<seed>& seeds)
{
	int64_t currMin = INT64_MAX;

	for (auto& seed : seeds)
	{
		if (seed.destination < currMin)
		{
			currMin = seed.destination;
		}
	}

	return currMin;
}

int64_t findClosestLocationRangedSeeds(const vector<seed>& seeds, const almanacMaps& almanacMaps)
{
	int64_t currMin = INT64_MAX;

	for (int i = 0; i < seeds.size(); i += 2)
	{
		int64_t currStart = seeds[i].number;
		int64_t currRange = seeds[i+1].number;

		for (int64_t j = currStart; j < currStart + currRange; j++)
		{
			seed currSeed = { j, j };

			findLocationForSeed(currSeed, almanacMaps);

			if (currSeed.destination < currMin)
			{
				currMin = currSeed.destination;
			}
		}
	}

	return currMin;
}

void readFromIntput(const string inputPath, vector<seed>& seeds, almanacMaps& almanacMaps)
{	
	ifstream inputFile;
	string line;

	inputFile.open(inputPath);

	if (inputFile.is_open())
	{
		int currState = SEEDS;

		while (getline(inputFile, line))
		{
			if (line == "")
			{
				currState++;
				continue;
			}		
			
			// skip labels that includes "map:"
			if (line.find("map:") != std::string::npos)
			{
				continue;
			}

			switch (currState)
			{
			case SEEDS:
				line = line.substr(7, line.size()); // 7 is legth of string "seeds: "
				processSeedsToVector(line, seeds);
				break;

			case SOIL:
				almanacMaps.seedToSoil.push_back(lineToConversion(line));
				break;

			case FERTILIZER:
				almanacMaps.soilToFertilizer.push_back(lineToConversion(line));
				break;

			case WATER:
				almanacMaps.fertilizerToWater.push_back(lineToConversion(line));
				break;

			case LIGHT:
				almanacMaps.waterToLight.push_back(lineToConversion(line));
				break;

			case TEMPERATURE:
				almanacMaps.lightToTemperature.push_back(lineToConversion(line));
				break;

			case HUMIDITY:
				almanacMaps.temperatureToHumidity.push_back(lineToConversion(line));
				break;

			case LOCATION:
				almanacMaps.humidityToLocation.push_back(lineToConversion(line));
				break;

			default:
				break;
			}
		}
	}
	else
	{
		cout << "Failed to open the input file.\n";
	}

	inputFile.close();
}

int main()
{
	const string inputPath = "C:\\input.txt";
	vector<seed> seeds;
	almanacMaps almanacMaps;

	readFromIntput(inputPath, seeds, almanacMaps);
	findLocationForSeeds(seeds, almanacMaps);

	cout << "      closestLocationSeeds : " << findClosestLocation(seeds) << "\n";
	cout << "closestLocationRangedSeeds : " << findClosestLocationRangedSeeds(seeds, almanacMaps) << "\n";

	return 0;
}
