#include "printer.h"

void printSeeds(const std::vector<seed> seeds)
{
	std::cout << "seeds:\n";

	for (auto& seed : seeds)
	{
		std::cout << "  " << seed.number << " -> " << seed.destination << "\n";
	}

	std::cout << "\n";
}

void printAlmanacMap(const std::vector<conversion> AlmanacMap)
{
	for (auto& conversion : AlmanacMap)
	{
		std::cout << "  " << conversion.destination << " " << conversion.source << " " << conversion.range << "\n";
	}
}

void printAlmanacMaps(const almanacMaps almanacMaps)
{
	std::cout << "seed-to-soil:\n";
	printAlmanacMap(almanacMaps.seedToSoil);

	std::cout << "\nsoil-to-fertilizer:\n";
	printAlmanacMap(almanacMaps.soilToFertilizer);

	std::cout << "\nfertilizer-to-water:\n";
	printAlmanacMap(almanacMaps.fertilizerToWater);

	std::cout << "\nwater-to-light:\n";
	printAlmanacMap(almanacMaps.waterToLight);

	std::cout << "\nlight-to-temperature:\n";
	printAlmanacMap(almanacMaps.lightToTemperature);

	std::cout << "\ntemperature-to-humidity:\n";
	printAlmanacMap(almanacMaps.temperatureToHumidity);

	std::cout << "\nhumidity-to-location:\n";
	printAlmanacMap(almanacMaps.humidityToLocation);

	std::cout << "\n";
}
