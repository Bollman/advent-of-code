#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "constsEnumsStructs.h"
#include "printer.h"

using namespace std;

bool isTreeVisibleFromDirection(vector< vector<int> >& treeGrid, int line, int column, int direction)
{
	const int treeHeigth = treeGrid[line][column];	

	switch (direction)
	{
	case NORTH :
		for (int i = line-1; i >= 0; i--)
		{
			if (treeHeigth <= treeGrid[i][column])
				return false;
		}
		break;

	case EAST :
		for (int j = column + 1; j < treeGrid[line].size(); j++)
		{
			if (treeHeigth <= treeGrid[line][j])
				return false;
		}
		break;

	case SOUTH :
		for (int i = line + 1; i < treeGrid.size(); i++)
		{
			if (treeHeigth <= treeGrid[i][column])
				return false;
		}
		break;

	case WEST :
		for (int j = column -1; j >= 0; j--)
		{
			if (treeHeigth <= treeGrid[line][j])
				return false;
		}
		break;
	}

	return true;
}

bool isTreeVisible(vector< vector<int> >& treeGrid, int line, int column)
{
	if (isTreeVisibleFromDirection(treeGrid, line, column, NORTH))
		return true;

	if (isTreeVisibleFromDirection(treeGrid, line, column, EAST))
		return true;

	if (isTreeVisibleFromDirection(treeGrid, line, column, SOUTH))
		return true;

	if (isTreeVisibleFromDirection(treeGrid, line, column, WEST))
		return true;

	return false;
}

int countVisibleTreesInterior(vector< vector<int> >& treeGrid)
{
	int visibleTreesCount = 0;

	for (int i = 1; i < treeGrid.size() - 1; i++)
	{
		for (int j = 1; j < treeGrid[i].size() - 1; j++)
		{
			if ( isTreeVisible(treeGrid, i, j) )
				visibleTreesCount++;
		}
	}

	return visibleTreesCount;
}

int countTreeGridCircuit(vector< vector<int> >& treeGrid)
{
	return 2 * (treeGrid.size() - 1) + 2 * (treeGrid[0].size() - 1);
}

int countTreeScenicScore(vector< vector<int> >& treeGrid, int line, int column)
{
	const int treeHeigth = treeGrid[line][column];
	int viewCountNorth = 0;
	int viewCountEast = 0;
	int viewCountSouth = 0;
	int viewCountWest = 0;

	//NORTH
	if (line > 0)
	{
		for (int i = line-1; i >=0; i--)
		{
			if (treeHeigth > treeGrid[i][column])
			{
				viewCountNorth++;
				continue;
			}
		
			viewCountNorth++;
			break;
		}
	}	

	//EAST
	if (column < treeGrid[line].size())
	{
		for (int j = column + 1; j < treeGrid[line].size(); j++)
		{
			if (treeHeigth > treeGrid[line][j])
			{
				viewCountEast++;
				continue;
			}

			viewCountEast++;
			break;
		}

	}

	//SOUTH
	if (line < treeGrid.size())
	{
		for (int i = line + 1; i < treeGrid.size(); i++)
		{
			if (treeHeigth > treeGrid[i][column])
			{
				viewCountSouth++;
				continue;
			}

			viewCountSouth++;
			break;
		}
	}

	//WEST
	if (column > 0)
	{
		for (int j = column - 1; j >= 0; j--)
		{
			if (treeHeigth > treeGrid[line][j])
			{
				viewCountWest++;
				continue;
			}

			viewCountWest++;
			break;
		}

	}

	return viewCountNorth * viewCountEast * viewCountSouth * viewCountWest;
}

int countHighestGridScenicScore(vector< vector<int> >& treeGrid)
{
	int highestScore = 0;
	int currHighestScore = 0;

	for ( int i = 0; i < treeGrid.size(); i++)
	{
		for (int j = 0; j < treeGrid[0].size(); j++)
		{
			currHighestScore = countTreeScenicScore(treeGrid, i, j);

			if (currHighestScore > highestScore)
				highestScore = currHighestScore;
		}
	}

	return highestScore;
}

void readFromIntput(string inputPath, vector< vector<int> >& treeGrid)
{
	ifstream inputFile;
	string line;

	inputFile.open(inputPath);

	if (inputFile.is_open())
	{
		while (getline(inputFile, line))
		{
			vector<int> currLineVector;
			
			for (int i = 0; i < line.length(); i++ )
			{
				currLineVector.push_back( int(line[i]) - asciiNumberOffset);
			}

			treeGrid.push_back(currLineVector);
		}
	}
	else
	{
		cout << "Failed to open the input file.\n";
	}

	inputFile.close();
}

int main()
{
	const string inputFilePath = "C:\input.txt";
	vector< vector<int> > treeGrid;

	readFromIntput(inputFilePath, treeGrid);

	cout << "    visibleTrees : " << countTreeGridCircuit(treeGrid) + countVisibleTreesInterior(treeGrid) << "\n";
	cout << "highestGridScene : " << countHighestGridScenicScore(treeGrid) << "\n";

	return 0;
}
