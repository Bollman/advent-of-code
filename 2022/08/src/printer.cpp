#include "printer.h"

void printTreeGrid(std::vector< std::vector<int> >& treeGrid)
{
	for (auto& line : treeGrid)
	{
		for (auto& heigth : line)
		{
			std::cout << heigth;
		}
		std::cout << "\n";
	}
}
