#pragma once

const int asciiNumberOffset = 48;

typedef enum
{
	NORTH,
	EAST,
	SOUTH,
	WEST

} Direction;