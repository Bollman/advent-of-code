#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "constsEnumsStructs.h"
#include "printer.h"

using namespace std;

/**
 * finds index of the crate on top of the line
 */
int findLineTop(vector< vector<char> >& transposedCratesStock, int lineNumber)
{
	for (int i = 0; i < transposedCratesStock[lineNumber].size(); i++)
	{
		if (transposedCratesStock[lineNumber][i] == '-')
		{
			if (i == 0)
			{
				return i;
			}
			else
			{
				return i - 1;
			}
		}
	}

	return transposedCratesStock[lineNumber].size() - 1;
}

void moveCrate(vector< vector<char> >& transposedCratesStock, int source, int destination)
{
	char movedCrate = transposedCratesStock[source].back();

	transposedCratesStock[destination].push_back(transposedCratesStock[source].back());
	transposedCratesStock[source].pop_back();

}

void moveCratesBundle(vector< vector<char> >& transposedCratesStock, int ammountOfCrates, int source, int destination)
{
	int index = transposedCratesStock[source].size() - ammountOfCrates;

	while (index < transposedCratesStock[source].size())
	{
		transposedCratesStock[destination].push_back(transposedCratesStock[source][index]);
		transposedCratesStock[source].erase(transposedCratesStock[source].begin() + index);
	}

}

void doProcedure(vector< vector<char> >& transposedCratesStock, PROCEDURE procedure, int craneType)
{
	const int crateLineOffset = 1;
	int source = procedure.sourceColumn - crateLineOffset;
	int destination = procedure.destinationColumn - crateLineOffset;

	if (craneType == OLDCRANE)
	{
		for (int i = 0; i < procedure.ammountOfCrates; i++)
		{
			moveCrate(transposedCratesStock, source, destination);
		}
	}
	else if (craneType == NEWCRANE)
	{
		moveCratesBundle(transposedCratesStock, procedure.ammountOfCrates, source, destination);
	}
}

void doAllProcedures(vector< vector<char> >& transposedCratesStock, vector<PROCEDURE>& rearrangementProcedures, int craneType)
{
	for (auto& procedure : rearrangementProcedures)
	{
		doProcedure(transposedCratesStock, procedure, craneType);
	}
}

void transposeCratesStock(vector< vector<char> >& cratesStock, vector< vector<char> >& transposedCratesStock)
{

	int numberOfLines = cratesStock.size();
	int numberOfColumns = cratesStock[0].size();

	// create empty lines in new matrix
	for (int i = 0; i < numberOfColumns; i++)
	{
		vector<char> emptyVector(0);
		transposedCratesStock.push_back(emptyVector);
	}

	for (int i = cratesStock.size() - 1; i >= 0; i--)
	{
		for (int j = 0; j < cratesStock[i].size(); j++)
		{
			if (cratesStock[i][j] != '-')
				transposedCratesStock[j].push_back(cratesStock[i][j]);
		}
	}
}

string readTopCratesMessage(vector< vector<char> >& transposedCratesStock)
{
	string message = "";

	for (int i = 0; i < transposedCratesStock.size(); i++)
	{
		if (transposedCratesStock[i].size() != 0)
		{
			message.push_back(transposedCratesStock[i].back());
		}
	}

	return message;
}

void readFromIntput(string inputPath, vector<PROCEDURE>& rearrangementProcedures, vector< vector<char> >& cratesStock)
{
	const int crateStringSize = 3;
	const string noCrateString = "   ";

	ifstream inputFile;
	string line;

	bool readingProcedure = false;
	int numberOfCratesColumns = 0;
	int currentCratesLine = 0;

	inputFile.open(inputPath);

	if (inputFile.is_open())
	{
		while (getline(inputFile, line))
		{

			if (line.find("[") != string::npos) // reading crates
			{
				vector<char> emptyVector(0);
				cratesStock.push_back(emptyVector);

				for (int i = 0; i < line.size(); i += 4)
				{
					if (line.substr(i, crateStringSize) == noCrateString)
					{
						cratesStock[currentCratesLine].push_back('-');
					}
					else
					{
						cratesStock[currentCratesLine].push_back(line[i + 1]);
					}
				}

				currentCratesLine++;
			}

			if (line.find("move") != string::npos) // reading procedure
			{
				const int cratesAmountIndex = 1;
				const int sourceColumnIndex = 3;
				const int destinationColumnIndex = 5;

				PROCEDURE procedure;

				stringstream ss(line);
				istream_iterator<string> begin(ss);
				istream_iterator<string> end;
				vector<string> tokens(begin, end);

				procedure.ammountOfCrates = stoi(tokens[cratesAmountIndex]);
				procedure.sourceColumn = stoi(tokens[sourceColumnIndex]);
				procedure.destinationColumn = stoi(tokens[destinationColumnIndex]);

				rearrangementProcedures.push_back(procedure);
			}
		}
	}
	else
	{
		cout << "Failed to open the input file.\n";
	}

	inputFile.close();
}

int main()
{
	const string inputFilePath = "C:\input.txt";

	vector< vector<char> > cratesStockCrane;
	vector< vector<char> > transposedCratesStockOldCrane;
	vector< vector<char> > transposedCratesStockNewCrane;
	vector<PROCEDURE> rearrangementProcedures;

	readFromIntput(inputFilePath, rearrangementProcedures, cratesStockCrane);
	transposeCratesStock(cratesStockCrane, transposedCratesStockOldCrane);
	transposedCratesStockNewCrane = transposedCratesStockOldCrane;

	doAllProcedures(transposedCratesStockOldCrane, rearrangementProcedures,OLDCRANE);
	doAllProcedures(transposedCratesStockNewCrane, rearrangementProcedures, NEWCRANE);

	cout << "topCratesMessageOldCrane : " << readTopCratesMessage(transposedCratesStockOldCrane) << "\n";
	cout << "topCratesMessageNewCrane : " << readTopCratesMessage(transposedCratesStockNewCrane) << "\n";

	return 0;
}
