#include "printer.h"

void printCrates(std::vector< std::vector<char> >& cratesStock)
{
	int numberOfLines = cratesStock.size();
	int numberOfColumns = cratesStock[0].size();

	for (int i = 0; i < cratesStock.size(); i++)
	{
		for (int j = 0; j < cratesStock[i].size(); j++)
		{
			std::cout << cratesStock[i][j];

			if (j < cratesStock[i].size() - 1)
			{
				std::cout << " ";
			}
			else
			{
				std::cout << "\n";
			}
		}
	}
}

void printTransposedCrates(std::vector< std::vector<char> >& transposedCratesStock)
{
	int numberOfLines = transposedCratesStock.size();
	int numberOfColumns = transposedCratesStock[0].size();

	for (int i = 0; i < transposedCratesStock.size(); i++)
	{
		for (int j = 0; j < transposedCratesStock[i].size(); j++)
		{
			std::cout << transposedCratesStock[i][j];

			if (j < transposedCratesStock[i].size() - 1)
			{
				std::cout << " ";
			}
			else
			{
				std::cout << "\n";
			}
		}
	}
}

void printProcedure(PROCEDURE procedure)
{
	std::cout << "move " << procedure.ammountOfCrates << " from " << procedure.sourceColumn << " to " << procedure.destinationColumn << "\n";
}

void printAllProcedures(std::vector<PROCEDURE>& rearrangementProcedures)
{
	for (auto& procedure : rearrangementProcedures)
	{
		printProcedure(procedure);
	}
}
