#pragma once
#include <iostream>
#include <vector>
#include "constsEnumsStructs.h"

void printCrates(std::vector< std::vector<char> >& cratesStock);

void printTransposedCrates(std::vector< std::vector<char> >& transposedCratesStock);

void printProcedure(PROCEDURE procedure);

void printAllProcedures(std::vector<PROCEDURE>& rearrangementProcedures);
