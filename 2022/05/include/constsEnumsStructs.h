#pragma once

const int columnOffset = 1;

typedef enum {

	OLDCRANE,
	NEWCRANE
}CraneType;

typedef struct
{
	int ammountOfCrates;
	int sourceColumn;
	int destinationColumn;

}PROCEDURE;
