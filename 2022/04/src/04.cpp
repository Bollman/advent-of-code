#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

void printParsedPairs(vector< vector<int> >& parsedPairs)
{
	for (auto& parsedPair : parsedPairs)
	{
		cout << parsedPair[0] << "\n";
		cout << parsedPair[1] << "\n";
		cout << parsedPair[2] << "\n";
		cout << parsedPair[3] << "\n\n";
	}
}

bool parsedPairIsOverlaping(vector<int> parsedPair)
{
	if (parsedPair[0] <= parsedPair[2] && parsedPair[1] >= parsedPair[3])
		return true;

	if (parsedPair[0] >= parsedPair[2] && parsedPair[1] <= parsedPair[3])
		return true;

	return false;
}

bool parsedPairIsPartialyOverlaping(vector<int> parsedPair)
{
	if (parsedPairIsOverlaping(parsedPair))
		return true;

	if (parsedPair[0] <= parsedPair[2] && parsedPair[1] >= parsedPair[2] && parsedPair[1] <= parsedPair[3])
		return true;

	if (parsedPair[0] >= parsedPair[2] && parsedPair[0] <= parsedPair[3] && parsedPair[1] >= parsedPair[3])
		return true;

	return false;
}

int countPartialOverlapsInPairs(vector< vector<int> >& parsedPairs)
{
	int partialOverlaps = 0;

	for (auto& parsedPair : parsedPairs)
	{
		if (parsedPairIsPartialyOverlaping(parsedPair))
			partialOverlaps++;
	}

	return partialOverlaps;
}

int countOverlapsInPairs(vector< vector<int> >& parsedPairs)
{
	int overlaps = 0;

	for (auto& parsedPair : parsedPairs)
	{
		if (parsedPairIsOverlaping(parsedPair))
			overlaps++;
	}

	return overlaps;
}

vector<int> parseLineToVector(string line)
{
	vector<int> parsedLine;
	int commaPos;
	int dashPos;

	commaPos = line.find(",");
	string firstSectorBorders = line.substr(0, commaPos);
	string secondSectorBorders = line.substr(commaPos + 1, line.size() - commaPos);

	dashPos = firstSectorBorders.find("-");
	parsedLine.push_back(stoi(firstSectorBorders.substr(0, dashPos)));
	parsedLine.push_back(stoi(firstSectorBorders.substr(dashPos + 1, line.size() - dashPos)));

	dashPos = secondSectorBorders.find("-");
	parsedLine.push_back(stoi(secondSectorBorders.substr(0, dashPos)));
	parsedLine.push_back(stoi(secondSectorBorders.substr(dashPos + 1, line.size() - dashPos)));

	return parsedLine;
}

void readPairsFromIntput(string inputPath, vector< vector<int> >& parsedPairs)
{
	ifstream inputFile;
	string line;

	inputFile.open(inputPath);

	if (inputFile.is_open())
	{
		while (getline(inputFile, line))
		{
			vector<int> parsedLine = parseLineToVector(line);
			parsedPairs.push_back(parsedLine);
		}
	}
	else
	{
		cout << "Failed to open the input file.\n";
	}

	inputFile.close();
}

int main()
{
	const string inputFilePath = "C:\input.txt";

	vector< vector<int> > parsedPairs;

	readPairsFromIntput(inputFilePath, parsedPairs);

	cout << "       overlapsInPairs : " << countOverlapsInPairs(parsedPairs) << "\n";
	cout << "partialOverlapsInPairs : " << countPartialOverlapsInPairs(parsedPairs) << "\n";

	return 0;
}
