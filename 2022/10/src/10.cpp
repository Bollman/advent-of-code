#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <tuple>
#include <vector>
#include "constsEnumsStructs.h"
#include "printer.h"

using namespace std;

bool isCycleChosen(const int cycle)
{
	for (auto& chosenCycle : chosenCycles)
		if (cycle == chosenCycle)
			return true;
	
	return false;
}

void getSignalStrengthsFromInstructions(const vector<Instruction>& instructions, vector<int>& signalStrengths)
{
	int cycleCount = 1;
	int registerX = registerXInitValue;

	for (auto& instruction : instructions)
	{
		switch (instruction.name)
		{
		case noop:
			if (isCycleChosen(cycleCount))
				signalStrengths.push_back(cycleCount * registerX);
			cycleCount++;
			break;

		case addx:
			if (isCycleChosen(cycleCount))
				signalStrengths.push_back(cycleCount * registerX);
			cycleCount++;

			if (isCycleChosen(cycleCount))
				signalStrengths.push_back(cycleCount * registerX);
			cycleCount++;

			registerX += instruction.parameter;
			break;
		}
	}	
}

int sumSignalStrengths(const vector<int>& signalStrengths)
{
	int sum = 0;

	for (auto& signal : signalStrengths)
		sum += signal;

	return sum;
}

tuple<int, int> cycleToPosition(const int cycle)
{
	//cycle counting starts at "1"
	return { (cycle - 1) / 40, (cycle - 1) % 40 };
}

bool isDrawedPixelCoveredBySprite(const int registerX, const int column)
{
	return (abs(registerX - column) <= 1);
}

void drawOnCrtMonitor(const vector<Instruction>& instructions, vector< vector<bool> >& crtMonitor)
{
	int cycleCount = 1;
	int registerX = registerXInitValue;
	tuple<int, int> drawedPixel; // tuple <row, column>

	for (int i = 0; i < crtHeigth; i++)
	{
		crtMonitor.push_back({});

		for (int j = 0; j < crtWidth; j++)
		{
			crtMonitor[i].push_back(false);
		}
	}

	for (auto& instruction : instructions)
	{
		switch (instruction.name)
		{
		case noop:
			drawedPixel = cycleToPosition(cycleCount); 
			if (isDrawedPixelCoveredBySprite(registerX, get<1>(drawedPixel)))
				crtMonitor[get<0>(drawedPixel)][get<1>(drawedPixel)] = true;
			cycleCount++;
			break;

		case addx:
			drawedPixel = cycleToPosition(cycleCount); 
			if (isDrawedPixelCoveredBySprite(registerX, get<1>(drawedPixel)))
				crtMonitor[get<0>(drawedPixel)][get<1>(drawedPixel)] = true;
			cycleCount++;

			drawedPixel = cycleToPosition(cycleCount); 
			if (isDrawedPixelCoveredBySprite(registerX, get<1>(drawedPixel)))
				crtMonitor[get<0>(drawedPixel)][get<1>(drawedPixel)] = true;
			cycleCount++;

			registerX += instruction.parameter;
			break;
		}
	}
}

void readFromIntput(const string inputPath, vector<Instruction>& instructions)
{
	ifstream inputFile;
	string line;
	string instructionName;

	inputFile.open(inputPath);

	if (inputFile.is_open())
	{
		while (getline(inputFile, line))
		{
			instructionName = line.substr(0, instructionNameLength);

			if (instructionName == "noop")
				instructions.push_back({ noop, 0});
			else if (instructionName == "addx")
				instructions.push_back({ addx, stoi(line.substr(instructionNameLength, line.length()- instructionNameLength)) });
		}
	}
	else
	{
		cout << "Failed to open the input file.\n";
	}

	inputFile.close();
}

int main()
{
	const string inputFilePath = "C:\\input.txt";
	vector<Instruction> instructions;
	vector<int> signalStrengths;
	vector< vector<bool> > crtMonitor;

	readFromIntput(inputFilePath, instructions);
	getSignalStrengthsFromInstructions(instructions, signalStrengths);
	drawOnCrtMonitor(instructions, crtMonitor);

	cout << "sumOfChosenSignalStrengths : " << sumSignalStrengths(signalStrengths) << "\n";	
	printCrtMonitor(crtMonitor);

	return 0;
}
