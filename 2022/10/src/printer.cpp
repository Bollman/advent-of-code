#include "printer.h"

void printInstruction(const Instruction& instruction)
{
	if (instruction.name == noop)
		std::cout << "noop\n";
	else if (instruction.name == addx)
		std::cout << "addx " << instruction.parameter << "\n";
}

void printInstructions(const std::vector<Instruction>& instructions)
{
	for (auto& instruction : instructions)
		printInstruction(instruction);
}

void printSignalStrengths(const std::vector<int>& signalStrengths)
{
	for (auto& signal : signalStrengths)
		std::cout << signal << "\n";
}

void printCrtMonitor(const std::vector<std::vector<bool>>& crtMonitor)
{
	for (int i = 0; i < crtMonitor.size(); i++)
	{
		for (int j = 0; j < crtMonitor[i].size(); j++)
		{
			if (crtMonitor[i][j])
				std::cout << "#";
			else
				std::cout << " ";
		}
		std::cout << "\n";
	}
}
