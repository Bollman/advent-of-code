#pragma once
#include <string>
#include <vector>

const int addxCycles = 2;
const int noopCycles = 1;
const int instructionNameLength = 4;
const int registerXInitValue = 1;
const std::vector<int> chosenCycles = {20, 60, 100, 140, 180, 220};

const int crtHeigth = 6;
const int crtWidth = 40;

enum InstructionType {
	
	addx,
	noop
};

struct Instruction {

	int name;
	int parameter;
};