#pragma once
#include <iostream>
#include <vector>
#include "constsEnumsStructs.h"

void printInstruction(const Instruction& instruction);

void printInstructions(const std::vector<Instruction>& instructions);

void printSignalStrengths(const std::vector<int>& signalStrengths);

void printCrtMonitor(const std::vector< std::vector<bool> >& crtMonitor);
