#include "tree.h"
#include "constsEnumsStructs.h"

Tree::Tree(FileSystemNode* root)
{
	this->root = root;
}

void Tree::printTree()
{
	printSubtree(this->root,0);
}

void Tree::printSubtree(FileSystemNode* topNode, int level)
{
	for (int i = 0; i < level; i++)
		std::cout << "  ";

	std::cout << "- " << topNode->getName();
	if (topNode->getType() == DIRTYPE)
		std::cout << " (dir)\n";
	if (topNode->getType() == FILETYPE)
		std::cout << " (file, size=" << topNode->getSize() << ")\n";

	for (int i = 0; i < topNode->getNumberOfChildren(); i++)
	{
		printSubtree(topNode->getChildByNumber(i), level + 1);
	}
}
