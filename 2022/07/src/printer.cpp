#include "printer.h"

void printBrowsingLines(std::vector<BROWSINGLINE>& browsingList)
{
	for (auto& line : browsingList)
	{
		switch (line.lineType)
		{
		case FILENAME:
			std::cout << line.size << " " << line.name << "\n";
			break;

		case CDCMD:
			std::cout << "$ cd " << line.cmdParameter << "\n";
			break;

		case DIRNAME:
			std::cout << "dir " << line.name << "\n";
			break;

		case LSCMD:
			std::cout << "$ ls\n";
		}
	}
}
