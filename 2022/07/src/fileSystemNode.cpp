#include "fileSystemNode.h"

FileSystemNode::FileSystemNode(std::string name, int type, int size, FileSystemNode* parrent)
{
	this->name = name;
	this->type = type;	
	this->size = size;
	this->parrent = parrent;
}

std::string FileSystemNode::getName()
{
	return this->name;
}

int FileSystemNode::getType()
{
	return this->type;
}

int FileSystemNode::getSize()
{
	if (this->type == FILETYPE)
		return this->size;

	return 0;
}

void FileSystemNode::addChild(FileSystemNode* child)
{
	child->setParrent(this);
	this->children.push_back(child);
}

int FileSystemNode::getNumberOfChildren()
{
	return this->children.size();
}

FileSystemNode* FileSystemNode::getChildByNumber(int number)
{
	if ( number < 0 || number >= this->children.size() )
		return nullptr;

	return this->children[number];
}

FileSystemNode* FileSystemNode::getChildByName(std::string name)
{
	for (auto& child : this->children)
		if (child->getName() == name)
			return child;	

	return nullptr;
}

FileSystemNode* FileSystemNode::getParrent()
{
	return this->parrent;
}

void FileSystemNode::setParrent(FileSystemNode* parrent)
{
	this->parrent = parrent;
}

int FileSystemNode::countDirSize()
{
	int dirSize = 0;

	if (this->type != DIRTYPE)
		return dirSize;

	for (auto& child : this->children)
	{
		if (child->getType() == FILETYPE)
			dirSize += child->getSize();
		else
			dirSize += child->countDirSize();
	}

	return dirSize;
}

int FileSystemNode::sumDirSizesToLimit(int limit)
{
	int sum = 0;
	int currDirSize;

	if (this->type != DIRTYPE)
		return sum;

	currDirSize = this->countDirSize();
	if (currDirSize <= limit)
		sum += currDirSize;

	for (auto& child : this->children)
	{
		if (child->getType() == DIRTYPE)
		{
			sum += child->sumDirSizesToLimit(limit);
		}
	}

	return sum;
}

FileSystemNode* FileSystemNode::findSmallestSufficientDir(int spaceNeeded)
{
	FileSystemNode* smallestDir = nullptr;
	int smallestDirSize = totalDiskSpace;

	if (this->countDirSize() < spaceNeeded)
		return smallestDir;

	smallestDir = this;
		
	if (this->countDirSize() == spaceNeeded)
		return smallestDir;
	
	for (auto& child : this->children)
	{
		
		FileSystemNode* currSmallestChild = child->findSmallestSufficientDir(spaceNeeded);

		if (currSmallestChild != nullptr)
		{
			int currSmallestChildSize = currSmallestChild->countDirSize();

			if ( (currSmallestChildSize >= spaceNeeded) && (currSmallestChildSize < smallestDirSize) )
			{
				smallestDir = currSmallestChild;
				smallestDirSize = currSmallestChildSize;
			}
		}		
	}

	return smallestDir;
}

void FileSystemNode::printFullNode()
{
	std::cout << "nodeName : " << this->name << "\n";

	if (this->type == FILETYPE)
		std::cout << "fileType : FILETYPE" << "\n";
	if (this->type == DIRTYPE)
		std::cout << "fileType : DIRTYPE" << "\n";

	if (this->parrent != nullptr)
		std::cout << "nParrent : " << this->parrent->name << "\n";
	else
		std::cout << "nParrent : NULL\n";

	if (this->type == FILETYPE)
	std::cout << "nodeSize : " << this->size << "\n";

	if(this->type == DIRTYPE)
		this->printChildren();
	else
		std::cout << "\n";
}

void FileSystemNode::printChildren()
{
	std::cout << "children : ";

	for (int i = 0; i < this->children.size(); i++)
	{
		if(i==0)
			std::cout << this->children[i]->name << "\n";
		else
			std::cout << "           " << this->children[i]->name << "\n";
	}

	std::cout << "\n";
}
