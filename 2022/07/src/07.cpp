#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "constsEnumsStructs.h"
#include "fileSystemNode.h"
#include "printer.h"
#include "tree.h"

using namespace std;

FileSystemNode* createTree(vector<BROWSINGLINE>& browsingList)
{
	FileSystemNode* root = new FileSystemNode("/", DIRTYPE);
	FileSystemNode* currDir = root;

	for (int i = 0; i < browsingList.size(); i++)
	{
		switch (browsingList[i].lineType)
		{
		case FILENAME:		
			currDir->addChild( new FileSystemNode(browsingList[i].name, FILETYPE, browsingList[i].size, currDir) );
			break;

		case CDCMD:
			if (browsingList[i].cmdParameter == "..")
			{
				currDir = currDir->getParrent();
			}
			else if (browsingList[i].cmdParameter == "/")
			{
				currDir = root;
			}
			else
			{
				if(currDir->getChildByName(browsingList[i].cmdParameter) != nullptr)
					currDir = currDir->getChildByName(browsingList[i].cmdParameter);
			}
			break;

		case DIRNAME:
			currDir->addChild(new FileSystemNode(browsingList[i].name, DIRTYPE, 0, currDir));
			break;

		case LSCMD:
			break;
		}
	}
	return root;
}

void readFromIntput(string inputPath, vector<BROWSINGLINE>& browsingList)
{
	ifstream inputFile;
	string line;

	inputFile.open(inputPath);

	if (inputFile.is_open())
	{
		while (getline(inputFile, line))
		{
			BROWSINGLINE browsingLine;

			if (line.substr(0, cmdLength) == "$ cd") 
			{
				browsingLine.lineType = CDCMD;
				browsingLine.cmdParameter = line.substr(cmdLength+1, line.length() - cmdLength-1);
			}
			else if (line.substr(0, cmdLength) == "$ ls")
			{
				browsingLine.lineType = LSCMD;
			}
			else if (line.substr(0, dirLength) == "dir")
			{
				browsingLine.lineType = DIRNAME;
				browsingLine.name = line.substr(dirLength+1, line.length() - dirLength);
			}
			else 
			{
				int separatorPos = line.find(" ");
				browsingLine.lineType = FILENAME;
				browsingLine.size = stoi( line.substr(0,separatorPos) );
				browsingLine.name = line.substr(separatorPos + 1, line.length() - separatorPos);
			}

			browsingList.push_back(browsingLine);
		}
	}
	else
	{
		cout << "Failed to open the input file.\n";
	}

	inputFile.close();
}

int main()
{
	const string inputFilePath = "C:\input.txt";

	int freeSpace;
	vector<BROWSINGLINE> browsingList;
	FileSystemNode* root;
	FileSystemNode* smallestSufficientDir;
	
	readFromIntput(inputFilePath,browsingList);
	root = createTree(browsingList);
	freeSpace = totalDiskSpace - root->countDirSize();	
	smallestSufficientDir = root->findSmallestSufficientDir(spaceForUpdate - freeSpace);

	cout << "   sumOfDirsToLimit "<< dirSizeLimit << " : " << root->sumDirSizesToLimit(dirSizeLimit) << "\n";
	if (smallestSufficientDir != nullptr)
		cout << "smallestSufficientDirSpace : " << smallestSufficientDir->countDirSize() << "\n";

	return 0;
}
