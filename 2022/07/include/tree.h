#pragma once
#include "fileSystemNode.h"

class Tree
{
private:
	FileSystemNode* root;
	
public:
	Tree(FileSystemNode* root);

	void printTree();

	void printSubtree(FileSystemNode* topNode, int level = 0);
};
