#pragma once

const int cmdLength = 4;
const int dirLength = 3;
const int dirSizeLimit = 100000;
const int totalDiskSpace = 70000000;
const int spaceForUpdate = 30000000;

typedef struct {

	int lineType; // command, directory or file
	std::string cmdParameter;
	std::string name;
	int size;

}BROWSINGLINE;

enum FileType{

	FILETYPE,
	DIRTYPE
};

enum LineType {

	CDCMD,   // change directory command
	LSCMD,   // change directory command
	DIRNAME, // directory name
	FILENAME // file name
};
