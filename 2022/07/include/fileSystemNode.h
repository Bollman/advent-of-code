#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "constsEnumsStructs.h"

class FileSystemNode
{
private:
	std::string name;
	int type; // FILETYPE or DIRTYPE
	int size;
	FileSystemNode* parrent;
	std::vector<FileSystemNode*> children;

public:
	FileSystemNode(std::string name, int type, int size = 0, FileSystemNode* parrent = nullptr);

	std::string getName();

	int getType();

	int getSize();

	void addChild(FileSystemNode* child);

	int getNumberOfChildren();

	FileSystemNode* getChildByNumber(int number);

	FileSystemNode* getChildByName(std::string name);

	FileSystemNode* getParrent();

	void setParrent(FileSystemNode* parrent);

	int countDirSize();

	int sumDirSizesToLimit(int limit);

	FileSystemNode* findSmallestSufficientDir(int spaceNeeded);

	void printFullNode();

	void printChildren();
};
