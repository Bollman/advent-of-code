#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

void printElves(vector<vector<int>>& elves)
{
	for (auto & elf : elves)
	{
		for (auto & item : elf)
		{
			cout << item << "\n";
		}
		cout << "\n";
	}
	cout << "\n";
}

int findMaxCalories(vector<vector<int>>& elves)
{
	int maxCalories = 0;

	for (auto & elf:elves)
	{
		int carriedCalories = 0;

		for(auto & item:elf)
		{
			carriedCalories += item;
		}

		if (carriedCalories > maxCalories) {
			maxCalories = carriedCalories;
		}
	}

	return maxCalories;
}

int sumTopThreeCalories(vector<vector<int>>& elves)
{
	int top0 = 0;
	int top1 = 0;
	int top2 = 0;

	for (auto& elf : elves)
	{
		int carriedCalories = 0;

		for (auto& item : elf)
		{
			carriedCalories += item;
		}	

		if (carriedCalories > top0)
		{
			top2 = top1;
			top1 = top0;
			top0 = carriedCalories;
		}
		else if (carriedCalories > top1)
		{
			top2 = top1;
			top1 = carriedCalories;
		}
		else if (carriedCalories > top2)
		{
			top2 = carriedCalories;
		}		
	}

	return top0 + top1 + top2;
}

void readFromIntput(string inputPath, vector<vector<int>>& elves)
{
	ifstream inputFile;
	string line;

	inputFile.open(inputPath);

	if (inputFile.is_open())
	{
		int elfCounter = 0;

		while (inputFile) {

			getline(inputFile, line);

			if (line != "")
			{
				elves[elfCounter].push_back(stoi(line));
			}
			else
			{
				elfCounter++;
				elves.push_back({});
			}
		}
	}
	else
	{
		cout << "Failed to open the input file.\n";
	}

	elves.pop_back();
	inputFile.close();
}

int main()
{
	const string inputFilePath = "C:\input.txt";

	vector<vector<int>> elves = { {} };

	readFromIntput(inputFilePath, elves);	
	cout << "     maxCalories : " << findMaxCalories(elves) << "\n";
	cout << "topThreeCalories : " << sumTopThreeCalories(elves) << "\n";
}
