#pragma once

const int startingLine = 0;
const int startingColumn = 0;
const int distanceInStepOffset = 2;
const int asciiNumberOffset = 48;
const int shortRopeLength = 2;
const int longRopeLength = 10;

enum DIRECTION {

	UP,
	DOWN,
	LEFT,
	RIGHT
};

struct Step {

	int direction;
	int distance;
};

struct Position {

	int line;
	int column;

	bool operator==(const Position& a) const
	{
		return line == a.line && column == a.column;
	}

	bool operator<(const Position& a) const
	{
		return ( (line < a.line) || (line == a.line && column < a.column) );
	}	
};
