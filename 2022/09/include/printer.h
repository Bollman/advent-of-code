#pragma once
#include <algorithm> 
#include <iostream>
#include <set>
#include <vector>
#include "constsEnumsStructs.h"

void printStep(const Step step);

void printSteps(const std::vector<Step>& steps);

void printRopePositions(const std::vector<Position> rope);

bool positionWasVisitedByTail(const Position position, const std::set<Position>& tailVisitedPositions);

void printPositionsVisitedByTail(const std::set<Position>& tailVisitedPositions);
