#include "printer.h"

void printStep(const Step step)
{
	switch (step.direction)
	{
	case UP:
		std::cout << "U " << step.distance << "\n";
		break;

	case RIGHT:
		std::cout << "R " << step.distance << "\n";
		break;

	case DOWN:
		std::cout << "D " << step.distance << "\n";
		break;

	case LEFT:
		std::cout << "L " << step.distance << "\n";
		break;
	}
}

void printSteps(const std::vector<Step>& steps)
{
	for (auto& step : steps)
	{
		printStep(step);
	}
}

void printRopePositions(const std::vector<Position> rope)
{
	int minLine = startingLine;
	int maxLine = startingLine;
	int minColumn = startingColumn;
	int maxColumn = startingColumn;

	for (int i = 0; i < rope.size(); i++)
	{
		if (rope[i].line < minLine)
			minLine = rope[i].line;
		if (rope[i].line > maxLine)
			maxLine = rope[i].line;

		if (rope[i].column < minColumn)
			minColumn = rope[i].column;
		if (rope[i].column > maxColumn)
			maxColumn = rope[i].column;
	}

	for (int i = maxLine; i >= minLine; i--)
	{
		for (int j = minColumn; j <= maxColumn; j++)
		{			
			if (rope.size() == shortRopeLength)
			{
				if (i == rope[0].line && j == rope[0].column)
					std::cout << "H";
				else if (i == rope[1].line && j == rope[1].column)
					std::cout << "T";
				else if (i == startingLine && j == startingColumn)
					std::cout << "S";
				else
					std::cout << ".";
			}
			else
			{
				Position ropePart = {i,j};
				auto ropePartIterator = std::find(rope.begin(), rope.end(), ropePart);
				int ropepartIndex = std::distance(rope.begin(), ropePartIterator);

				if (ropePartIterator != rope.end())
				{
					if (ropepartIndex == 0)
						std::cout << "H";
					else
						std::cout << ropepartIndex;
				}
				else if (i == startingLine && j == startingColumn)
					std::cout << "S";
				else
					std::cout << ".";
			}
		}
		std::cout << "\n";
	}
}

bool positionWasVisitedByTail(const Position position, const std::set<Position>& tailVisitedPositions)
{
	for (auto& tailposition : tailVisitedPositions)
	{
		if (position.line == tailposition.line && position.column == tailposition.column)
			return true;
	}

	return false;
}

void printPositionsVisitedByTail(const std::set<Position>& tailVisitedPositions)
{
	int minLine = startingLine;
	int maxLine = startingLine;
	int minColumn = startingColumn;
	int maxColumn = startingColumn;

	for (auto& position : tailVisitedPositions)
	{
		if (position.line < minLine)
			minLine = position.line;
		if (position.line > maxLine)
			maxLine = position.line;

		if (position.column < minColumn)
			minColumn = position.column;
		if (position.column > maxColumn)
			maxColumn = position.column;
	}

	for (int i = maxLine; i >= minLine; i--)
	{
		for (int j = minColumn; j <= maxColumn; j++)
		{
			if ((i == startingLine && j == startingColumn) && (positionWasVisitedByTail({ i,j }, tailVisitedPositions)))
				std::cout << "$";
			else if (positionWasVisitedByTail({ i,j }, tailVisitedPositions))
				std::cout << "#";
			else if (i == startingLine && j == startingColumn)
				std::cout << "S";
			else
				std::cout << ".";
		}
		std::cout << "\n";
	}
}
