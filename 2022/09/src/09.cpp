#include <iostream>
#include <fstream>
#include <set>
#include <sstream>
#include <string>
#include "constsEnumsStructs.h"
#include "printer.h"

using namespace std;

bool isLeaderTouchingFollower(const Position& leader, const Position& follower)
{
	return (abs(leader.line - follower.line) <= 1) && (abs(leader.column - follower.column) <= 1);
}

void moveFollowerToLeader(const Position& leader, Position& follower)
{
	if (std::abs(leader.line - follower.line) >= 2) {
		follower.line += (leader.line - follower.line) / 2;
		if (std::abs(leader.column - follower.column) >= 2)
			follower.column += (leader.column - follower.column) / 2;
		else
			follower.column += leader.column - follower.column;
	}
	else if (std::abs(leader.column - follower.column) >= 2) {
		follower.column += (leader.column - follower.column) / 2;
		follower.line += leader.line - follower.line;
	}
}

void executeStep(const int direction, Position& leader)
{
	switch (direction)
	{
	case UP:
		leader.line++;
		break;

	case RIGHT:
		leader.column++;
		break;

	case DOWN:
		leader.line--;
		break;

	case LEFT:
		leader.column--;
		break;
	}
}

void executeSteps(const vector<Step>& steps, const int ropeLenght, set<Position>& tailVisitedPositions)
{
	vector<Position> rope;

	for (int i = 0; i < ropeLenght; i++)
		rope.push_back({ startingLine,startingColumn });	

	for (auto& step : steps)
	{
		for (int i = 0; i < step.distance; i++)
		{
			executeStep(step.direction, rope[0]);

			for (int j = 0; j < ropeLenght-1; j++)
			{
				if (!isLeaderTouchingFollower(rope[j], rope[j + 1]))
					moveFollowerToLeader(rope[j], rope[j + 1]);

				if (j == ropeLenght - 2)
					tailVisitedPositions.insert(rope[j + 1]);
			}
		}	
	}
}

void readFromIntput(const string inputPath, vector<Step>& steps)
{
	ifstream inputFile;
	string line;
	Step step;

	inputFile.open(inputPath);

	if (inputFile.is_open())
	{
		while (getline(inputFile, line))
		{
			if (line[0] == 'U')
				step.direction = UP;

			if (line[0] == 'R')
				step.direction = RIGHT;

			if (line[0] == 'D')
				step.direction = DOWN;

			if (line[0] == 'L')
				step.direction = LEFT;

			step.distance = stoi(line.substr(distanceInStepOffset, line.length() - distanceInStepOffset));
			steps.push_back(step);
		}
	}
	else
	{
		cout << "Failed to open the input file.\n";
	}

	inputFile.close();
}

int main()
{
	const string inputFilePath = "C:\\input.txt";
	vector<Step> steps;
	set<Position> tailVisitedPositionsShortRope = { {startingLine,startingColumn} };
	set<Position> tailVisitedPositionsLongRope = { {startingLine,startingColumn} };

	readFromIntput(inputFilePath, steps);	
	executeSteps(steps, shortRopeLength, tailVisitedPositionsShortRope);
	executeSteps(steps, longRopeLength, tailVisitedPositionsLongRope);

	std::cout << "positionsVisitedByShortRopeTail : " << tailVisitedPositionsShortRope.size() << "\n";
	std::cout << " positionsVisitedByLongRopeTail : " << tailVisitedPositionsLongRope.size() << "\n";

	return 0;
}
