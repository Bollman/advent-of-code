#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "constsAndEnums.h"

using namespace std;

void printAllRuckSacks(vector <string>& allRucksacks)
{
	for (auto& rucksack : allRucksacks)
	{
		cout << rucksack << "\n";
	}
}

int getItemPriority(char item)
{
	if (int(item) <= UPPERCASELIMIT)
		return int(item) - UPPERCASEOFFSET;

	return int(item) - LOWERCASEOFFSET;
}

int countRucksackPriority(string rucksack)
{
	int ruckSackLength = rucksack.length();
	string firstPart = rucksack.substr(0, (ruckSackLength / 2));
	string secondPart = rucksack.substr((ruckSackLength / 2), (ruckSackLength / 2));
	char sameItem;
	bool sameItemFound = false;

	for (auto& i : firstPart)
	{
		for (auto& j : secondPart)
		{
			if (i == j)
			{
				sameItemFound = true;
				sameItem = i;
				break;
			}
		}

		if (sameItemFound)
			break;
	}

	if (sameItemFound)
		return getItemPriority(sameItem);
	else
		return 0;
}

int sumAllpriorities(vector <string>& allRucksacks)
{
	int sum = 0;

	for (auto& rucksack : allRucksacks)
	{
		sum += countRucksackPriority(rucksack);
	}

	return sum;
}

int countGroupPriority(string group0, string group1, string group2)
{
	char sameItem;
	bool sameItemFound = false;

	for (auto& i : group0)
	{
		for (auto& j : group1)
		{
			for (auto& k : group2)
			{
				if (i == j && j == k)
				{
					sameItemFound = true;
					sameItem = i;
					break;
				}

			}

			if (sameItemFound)
				break;
		}

		if (sameItemFound)
			break;
	}

	if (sameItemFound)
		return getItemPriority(sameItem);
	else
		return 0;
}

int sumPrioritiesByGroups(vector <string>& allRucksacks)
{
	int groupPrioritiesSum = 0;

	for (int i = 0; i < allRucksacks.size(); i += 3)
	{
		groupPrioritiesSum += countGroupPriority(allRucksacks[i], allRucksacks[i + 1], allRucksacks[i + 2]);
	}

	return groupPrioritiesSum;
}

void readFromIntput(string inputPath, vector <string>& allRucksacks)
{
	ifstream inputFile;
	string line;

	inputFile.open(inputPath);

	if (inputFile.is_open())
	{
		while (getline(inputFile, line))
		{
			allRucksacks.push_back(line);
		}
	}
	else
	{
		cout << "Failed to open the input file.\n";
	}

	inputFile.close();
}

int main()
{
	const string inputFilePath = "C:\input.txt";

	vector<string> allRucksacks;

	readFromIntput(inputFilePath, allRucksacks);

	cout << "     allPrioritiesSum : " << sumAllpriorities(allRucksacks) << "\n";
	cout << "prioritiesByGroupsSum : " << sumPrioritiesByGroups(allRucksacks) << "\n";

	return 0;
}
