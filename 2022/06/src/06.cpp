#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

bool stringIsDistinct(string potentialmarker)
{
	for (int i = 0; i < potentialmarker.size(); i++)
	{
		for (int j = i + 1; j < potentialmarker.size(); j++)
		{
			if (potentialmarker[i] == potentialmarker[j])
				return false;
		}
	}

	return true;
}

int findFirstVariableMarkerPostition(string input, int markerLength)
{
	for (int i = 0; i < input.size() - markerLength + 1; i++)
	{
		string potentialmarker = input.substr(i, markerLength);

		if (stringIsDistinct(potentialmarker))
			return i + markerLength;
	}

	return 0;
}

string readFromIntput(string inputPath)
{
	ifstream inputFile;
	string line;

	inputFile.open(inputPath);

	if (inputFile.is_open())
	{
		while (getline(inputFile, line))
		{
			return line;
		}
	}
	else
	{
		cout << "Failed to open the input file.\n";
	}

	inputFile.close();
}

int main()
{
	const string inputFilePath = "C:\input.txt";
	const int shortMarkerLength = 4;
	const int longMarkerLength = 14;

	string input = readFromIntput(inputFilePath);

	cout << "firstShortMarkerPosition : " << findFirstVariableMarkerPostition(input, shortMarkerLength) << "\n";
	cout << " firstLongMarkerPosition : " << findFirstVariableMarkerPostition(input, longMarkerLength) << "\n";
}
