#pragma once

const int pointsForLose = 0;
const int pointsForDraw = 3;
const int pointsForWin = 6;

enum strategyType
{
	OLDSTRATEGY,
	NEWSTRATEGY
};

enum symbol
{
	ROCK = 1,
	PAPER,
	SCISSORS
};

enum symbolResult
{
	LOSE = 1,
	DRAW,
	WIN
};
