#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "constsAndEnums.h"

using namespace std;

void printStrategy(vector <string>& strategy)
{
	for (auto& line : strategy)
	{
		cout << line << "\n";
	}
}

int symbolToValue(char symbol)
{
	if (symbol == 'A' || symbol == 'X')
		return ROCK;

	if (symbol == 'B' || symbol == 'Y')
		return PAPER;

	if (symbol == 'C' || symbol == 'Z')
		return SCISSORS;

	return 0;
}

int countOutcome(char opponentsSymbol, char mySymbol)
{
	if (opponentsSymbol == mySymbol)
		return pointsForDraw;

	if (
		(opponentsSymbol == SCISSORS && mySymbol == ROCK) ||
		(opponentsSymbol == ROCK && mySymbol == PAPER) ||
		(opponentsSymbol == PAPER && mySymbol == SCISSORS)
		)
		return pointsForWin;

	return pointsForLose;
}

int countOutcomeNewStrategy(char opponentsSymbol, char mySymbol)
{
	if (mySymbol == LOSE)
	{
		switch (opponentsSymbol)
		{
		case ROCK:
			return SCISSORS;

		case PAPER:
			return ROCK;

		case SCISSORS:
			return PAPER;
		}
	}
	else if (mySymbol == DRAW)
	{
		return opponentsSymbol + pointsForDraw;
	}
	else if (mySymbol == WIN)
	{
		switch (opponentsSymbol)
		{
		case ROCK:
			return PAPER + pointsForWin;

		case PAPER:
			return SCISSORS + pointsForWin;

		case SCISSORS:
			return ROCK + pointsForWin;
		}
	}

	return 0;
}

int countPointsFromRound(string round, int strategyType)
{
	const int opponentSymbolPosition = 0;
	const int mySymbolPosition = 2;

	int opponentsSymbol = symbolToValue(round[opponentSymbolPosition]);
	int mySymbol = symbolToValue(round[mySymbolPosition]);

	if (strategyType == OLDSTRATEGY)
		return mySymbol + countOutcome(opponentsSymbol, mySymbol);

	else if (strategyType == NEWSTRATEGY)
		return countOutcomeNewStrategy(opponentsSymbol, mySymbol);

	return 0;
}

int countPointsFromStrategy(int strategyType, vector <string>& strategy)
{
	int totalPoints = 0;

	for (auto& line : strategy)
	{
		totalPoints += countPointsFromRound(line, strategyType);
	}

	return totalPoints;
}

void readFromIntput(string inputPath, vector <string>& strategy)
{
	ifstream inputFile;
	string line;

	inputFile.open(inputPath);

	if (inputFile.is_open())
	{
		while (getline(inputFile, line))
		{
			strategy.push_back(line);
		}
	}
	else
	{
		cout << "Failed to open the input file.\n";
	}

	inputFile.close();
}

int main()
{
	const string inputFilePath = "C:\input.txt";
	vector <string> strategy;

	readFromIntput(inputFilePath, strategy);
	cout << "totalPoints : " << countPointsFromStrategy(OLDSTRATEGY, strategy) << "\n";
	cout << "totalPoints : " << countPointsFromStrategy(NEWSTRATEGY, strategy) << "\n";

	return 0;
}
