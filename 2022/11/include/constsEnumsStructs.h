#pragma once
#include <string>
#include <vector>

const std::string strMonkey = "Monkey ";
const std::string strItems = "  Starting items: ";
const std::string strOperation = "  Operation: new = old ";
const std::string strTest = "  Test: divisible by ";
const std::string strTrue = "    If true: throw to monkey ";
const std::string strFalse = "    If false: throw to monkey ";

const int numberOfRounds = 20;

enum Operation {

	ADD,
	MUL,
};

struct Monkey {

	std::vector<int> items;
	int operationType;
	std::string operationParameter;

	int divisibleValue;
	int targetMonkeyIfTrue;
	int targetMonkeyIfFalse;
	int inspectionCount;
};
