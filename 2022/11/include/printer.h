#pragma once
#include <iostream>
#include <vector>
#include "constsEnumsStructs.h"

void printMonkey(const Monkey monkey, const int monkeyNumber);

void printMonkeys(const std::vector<Monkey>& monkeys);

void printMonkeyInspectionCount(const Monkey monkey, const int monkeyNumber);

void printMonkeysInspectionCounts(const std::vector<Monkey>& monkeys);
