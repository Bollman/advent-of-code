#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include "constsEnumsStructs.h"
#include "printer.h"

using namespace std;

void doRound(vector<Monkey>& monkeys)
{
	int item;
	int currParameter;

	for (auto& monkey : monkeys)
	{
		for (auto& i : monkey.items)
		{
			
			// get first element
			item = monkey.items.front();
			monkey.inspectionCount++;
			monkey.items.erase(monkey.items.begin());

			// do operation with item
			if (monkey.operationParameter == "old")
				currParameter = item;
			else
				currParameter = stoi(monkey.operationParameter);

			if (monkey.operationType == ADD)
				item += currParameter;
			else if (monkey.operationType == MUL)
				item *= currParameter;

			//monkey gets bored
			item = item / 3;

			// check divisibility
			if (item % monkey.divisibleValue == 0)
				monkeys[monkey.targetMonkeyIfTrue].items.push_back(item);
			else
				monkeys[monkey.targetMonkeyIfFalse].items.push_back(item);
		}
	}
}

void doRounds(vector<Monkey>& monkeys, const int roundsCount)
{
	for (int i = 0; i < roundsCount; i++)
		doRound(monkeys);
}

int countMonkeyBusiness(vector<Monkey>& monkeys)
{
	int firstMax = 0;
	int secondMax = 0;

	for (auto& monkey : monkeys)
	{
		if (monkey.inspectionCount > firstMax)
		{
			secondMax = firstMax;
			firstMax = monkey.inspectionCount;
		}
		else if (monkey.inspectionCount > secondMax)
			secondMax = monkey.inspectionCount;
	}

	return firstMax * secondMax;
}

vector<int> itemsToVector(const string list)
{
	vector<int> items = {};
	string number = "";

	for (int i = 0; i < list.size(); i++)
	{
		if (list[i] >= '0' && list[i] <= '9')
		{
			number += list[i];
			continue;
		}
		else if (list[i] == ',')
		{
			items.push_back(stoi(number));
			number = "";
		}
	}

	items.push_back(stoi(number));
	return items;
}

void readFromIntput(const string inputPath, vector<Monkey>& monkeys)
{
	ifstream inputFile;
	string line;
	Monkey monkey;

	inputFile.open(inputPath);

	if (inputFile.is_open())
	{
		while (getline(inputFile, line))
		{
			if (line.rfind(strMonkey, 0) == 0)
				continue;
			else if (line.rfind(strItems, 0) == 0)
				monkey.items = itemsToVector(line.substr(strItems.length(), line.length() - strItems.length()));
			else if (line.rfind(strOperation, 0) == 0)
			{
				if (line[strOperation.length()] == '+')
					monkey.operationType = ADD;
				else if (line[strOperation.length()] == '*')
					monkey.operationType = MUL;
				monkey.operationParameter = line.substr(strOperation.length() + 2, line.length() - strOperation.length() -2);
			}
			else if (line.rfind(strTest, 0) == 0)
				monkey.divisibleValue = stoi( line.substr(strTest.length(), line.length() - strTest.length()));
			else if (line.rfind(strTrue, 0) == 0)
				monkey.targetMonkeyIfTrue = stoi(line.substr(strTrue.length(), line.length() - strTrue.length()));
			else if (line.rfind(strFalse, 0) == 0)
				monkey.targetMonkeyIfFalse = stoi(line.substr(strFalse.length(), line.length() - strFalse.length()));
			else
			{
				monkey.inspectionCount = 0;
				monkeys.push_back(monkey);
			}
		}
		monkey.inspectionCount = 0;
		monkeys.push_back(monkey);
	}
	else
	{
		cout << "Failed to open the input file.\n";
	}

	inputFile.close();
}

int main()
{
	const string inputFilePath = "C:\\input.txt";
	vector<Monkey> monkeys;

	readFromIntput(inputFilePath, monkeys);
	doRounds(monkeys, numberOfRounds);

	cout << "monkeyBusiness after " << numberOfRounds << " rounds : " << countMonkeyBusiness(monkeys) << "\n";

	return 0;
}
