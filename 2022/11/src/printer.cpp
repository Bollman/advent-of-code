#include "printer.h"

void printMonkey(const Monkey monkey, const int monkeyNumber)
{
	std::cout << strMonkey << monkeyNumber << "\n";

	std::cout << strItems;
	if (monkey.items.size() == 0)
		std::cout << "\n";
	for (int i = 0; i < monkey.items.size(); i++)
	{
		std::cout << monkey.items[i];
		if (i < monkey.items.size() - 1)
			std::cout << ", ";
		else
			std::cout << "\n";
	}

	std::cout << strOperation;
	if (monkey.operationType == ADD)
		std::cout << "+ " << monkey.operationParameter << "\n";
	else if (monkey.operationType == MUL)
		std::cout << "* " << monkey.operationParameter << "\n";

	std::cout << strTest << monkey.divisibleValue << "\n";
	std::cout << strTrue << monkey.targetMonkeyIfTrue << "\n";
	std::cout << strFalse << monkey.targetMonkeyIfFalse << "\n";
}

void printMonkeys(const std::vector<Monkey>& monkeys)
{
	for (int i = 0; i < monkeys.size(); i++)
	{
		printMonkey(monkeys[i], i);
		std::cout << "\n";
	}
}

void printMonkeyInspectionCount(const Monkey monkey, const int monkeyNumber)
{
	std::cout << "Monkey " << monkeyNumber << " inspected items " << monkey.inspectionCount << " times.\n";
}

void printMonkeysInspectionCounts(const std::vector<Monkey>& monkeys)
{
	for (int i = 0; i < monkeys.size(); i++)
		printMonkeyInspectionCount(monkeys[i], i);
}
